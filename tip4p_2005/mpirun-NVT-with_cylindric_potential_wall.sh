#!/bin/bash
#
#SBATCH -J NVT-with_wall
#SBATCH -N 5
#SBATCH --ntasks-per-node=24
#SBATCH --ntasks-per-core=1
#SBATCH --partition=mem_0096
#SBATCH --qos=mem_0096
#SBATCH --time=3:30:00

# when srun is used, you need to set:
#srun -l -N2 -n32 a.out
# or
#mpirun -np 32 a.out
# Use: np = N * ntasks-per-node
np=120

#source modules
module purge
module load intel-mkl/2019.5 intel-mpi/2019.5 intel/19.0.5 eigen/3.3.7-intel-19.0.5.281-fqkhf24 lammps/20190605-intel-19.0.5.281-v53g4r2

infile_1=${1}
varfile=${2}
. ${varfile}
radius=${infile_1##*radius_}
radius=${radius%%-*}
radius=${radius##*_}
echo "Found wall radius ${radius}"
#echo $kspace_1

# NVT-ensemble water in cylinder configuration with potential wall
mpirun -np $np lmp -in scripts/md-NVT-with_wall.lmp -var timestep ${timestep_1} -var t_start ${t_start_1} -var t_stop ${t_stop_1} -var t_damp ${t_damp_1} -var num_thermo ${num_thermo_1} -var num_run ${num_run_1} -var num_dump ${num_dump_1} -var infile "${infile_1}" -var model "${model_1}" -var kspace "${kspace_1}" -var radius_cyl ${radius} -var radius_adding ${radius_adding_for_potential_cutoff} -var potential_cutoff ${potential_cutoff} -var random $RANDOM

