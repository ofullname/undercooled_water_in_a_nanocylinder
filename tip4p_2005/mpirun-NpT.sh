#!/bin/bash
#
#SBATCH -J NpT-periodic-cube
#SBATCH -N 4
#SBATCH --ntasks-per-node=24
#SBATCH --ntasks-per-core=1
#SBATCH --partition=mem_0096
#SBATCH --qos=mem_0096
#SBATCH --time=1:30:00

# when srun is used, you need to set:
#srun -l -N2 -n32 a.out
# or
#mpirun -np 32 a.out
np=96

#source modules
module purge
module load intel-mkl/2019.5 intel-mpi/2019.5 intel/19.0.5 eigen/3.3.7-intel-19.0.5.281-fqkhf24 lammps/20190605-intel-19.0.5.281-v53g4r2

infile_1=${1}
varfile=${2}
. ${varfile}

# NpT-ensemble water in cube with periodic boundaries
mpirun -np $np lmp -in scripts/md-NpT.lmp -var timestep ${timestep_1} -var t_start ${t_start_1} -var t_stop ${t_stop_1} -var t_damp ${t_damp_1} -var p_start ${p_start_1} -var p_stop ${p_stop_1} -var p_damp ${p_damp_1} -var num_thermo ${num_thermo_1} -var num_run ${num_run_1} -var num_dump ${num_dump_1} -var infile "${infile_1}" -var model "${model_1}" -var kspace "${kspace_1}" -var random $RANDOM

