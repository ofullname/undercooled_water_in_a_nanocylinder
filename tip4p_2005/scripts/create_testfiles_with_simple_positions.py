#!/usr/bin/python3
# coding=utf-8

import sys
import math
import io
import os

TEMPLATE_FILENAME = str(sys.argv[1])
NUMBER_MOLECULES_PER_EDGE_LENGTH = int(float(sys.argv[2]))
NUMBER_RUNS = int(float(sys.argv[3]))
os.path.isfile('./final_data_folder')

NUMBER_MOLECULES = int(float(NUMBER_MOLECULES_PER_EDGE_LENGTH)**3.0)
BOXSIZE=100.0
DISPLACEMENT=1.0
EXP_BOXSIZE="{:e}".format(BOXSIZE)

for i in range(0,NUMBER_RUNS):
   FILENAME=TEMPLATE_FILENAME+str(NUMBER_MOLECULES)+"-run_"+str(NUMBER_RUNS)+"."+str(i)+".testdata"
   if os.path.isfile(FILENAME):
      os.remove(FILENAME)
   with open(FILENAME,"a+") as F:
      F.write("ITEM: TIMESTEP\n")
      F.write(str(i)+"\n")
      F.write("ITEM: NUMBER OF ATOMS\n")
      F.write(str(NUMBER_MOLECULES)+"\n")
      F.write("ITEM: BOX BOUNDS pp pp pp\n")
      F.write("0.0000000000000000e+00 "+str(EXP_BOXSIZE)+"\n")
      F.write("0.0000000000000000e+00 "+str(EXP_BOXSIZE)+"\n")
      F.write("0.0000000000000000e+00 "+str(EXP_BOXSIZE)+"\n")
      #                     0   1    2 3 4 5 6  7  8  9 10 11 12 13 14 15 16 17 18  19  20  21
      F.write("ITEM: ATOMS id mol type q x y z vx vy vz ix iy iz xu yu zu xs ys zs xsu ysu zsu \n")
      for l in range(0, NUMBER_MOLECULES_PER_EDGE_LENGTH):
         for k in range(0, NUMBER_MOLECULES_PER_EDGE_LENGTH):
            for j in range(0, NUMBER_MOLECULES_PER_EDGE_LENGTH):
               X_POS=float(j+1)*BOXSIZE/float(NUMBER_MOLECULES_PER_EDGE_LENGTH)
               X_POS-=0.5*BOXSIZE/float(NUMBER_MOLECULES_PER_EDGE_LENGTH)
               X_POS+=float(i)*DISPLACEMENT
               
               Y_POS=float(k+1)*BOXSIZE/float(NUMBER_MOLECULES_PER_EDGE_LENGTH)
               Y_POS-=0.5*BOXSIZE/float(NUMBER_MOLECULES_PER_EDGE_LENGTH)
               Y_POS+=float(i)*DISPLACEMENT
               
               Z_POS=float(l+1)*BOXSIZE/float(NUMBER_MOLECULES_PER_EDGE_LENGTH)
               Z_POS-=0.5*BOXSIZE/float(NUMBER_MOLECULES_PER_EDGE_LENGTH)
               Z_POS+=float(i)*DISPLACEMENT
               
               XS_POS=X_POS/BOXSIZE
               YS_POS=Y_POS/BOXSIZE
               ZS_POS=Z_POS/BOXSIZE
               
               ATOM_NUMBER=j+1
               ATOM_NUMBER+=k*NUMBER_MOLECULES_PER_EDGE_LENGTH
               ATOM_NUMBER+=l*NUMBER_MOLECULES_PER_EDGE_LENGTH*NUMBER_MOLECULES_PER_EDGE_LENGTH
               
               LINE=str(ATOM_NUMBER)+" "+str(ATOM_NUMBER)+" 2 -1.1794 "
               LINE=LINE+str(X_POS)+" "+str(Y_POS)+" "+str(Z_POS)
               LINE=LINE+" vx vy vz"
               LINE=LINE+" ix iy iz"
               LINE=LINE+" "+str(X_POS)+" "+str(Y_POS)+" "+str(Z_POS)
               LINE=LINE+" "+str(XS_POS)+" "+str(YS_POS)+" "+str(ZS_POS)
               LINE=LINE+" "+str(XS_POS)+" "+str(YS_POS)+" "+str(ZS_POS)
               LINE=LINE+" \n"
               
               F.write(LINE)
      F.close()



