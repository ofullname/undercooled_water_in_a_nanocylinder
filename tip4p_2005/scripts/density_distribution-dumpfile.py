#!/usr/bin/python3
# coding=utf-8

import sys
import math
import io
import numpy
#import matplotlib.pyplot as plt

"""
calculate RADIUS dependent density distribution for Oxygen atoms in cylinders-shells
POSFILE = lammps dump file

the box dimensions are taken from the file header
ITEM: BOX BOUNDS pp pp pp
information in the LINEs following the header have to look like this:
ITEM: ATOMS id type x y z vx vy vz ix iy iz

H2O molecular density should be ~1/29.915 [Molecules/Å³] for density of 1 [kg/dm³]

RADIUS_CYL_MINUS_CUTOFF: the cylinder potential RADIUS minus the potential cutoff
RADIUS: RADIUS_CYL_MINUS_CUTOFF plus arbitrary length, since atoms are allowed to enter potential, but potential strength and other parameters are not accessible by this script
RADIUS_SPLIT: in how many segments the RADIUS should be split
i.e. the thickness of the cylinder shells = RADIUS / RADIUS_SPLIT
"""

POSFILE = sys.argv[1]
RADIUS_CYL_MINUS_CUTOFF = float(sys.argv[2])
RADIUS_SPLIT = float(sys.argv[3])
NUMBER_DENSITY = numpy.zeros(int(numpy.ceil(RADIUS_SPLIT)))
ATOM_DENSITY = numpy.zeros(int(numpy.ceil(RADIUS_SPLIT)))
ATOM_COUNTER = numpy.zeros(int(numpy.ceil(RADIUS_SPLIT)))
PARTIAL_RADIUS_ARRAY = numpy.zeros(int(numpy.ceil(RADIUS_SPLIT)))
RADIUS = RADIUS_CYL_MINUS_CUTOFF
#RADIUS_MAX = RADIUS_CYL_MINUS_CUTOFF
print "Found file", POSFILE
print "Found radius", RADIUS_CYL_MINUS_CUTOFF
print "Splitting it", RADIUS_SPLIT, "times"

# get box size and cylinder axis from lammps data file
# get oxygen atoms position (ID=2) from lammps data file
# calculate distance from cylinder axis to oxygen atom
print "\nReading data from file."
XFROMHERE=False
YFROMHERE=False
ZFROMHERE=False
FROMHERE=False
XY_SCATTERPLOT_LIST = []
CYL_HEIGHT = 0.0
with open(POSFILE, 'r') as POSF:
#    print POSF
    for LINE in POSF:
        if ( FROMHERE ):
            if ( LINE.split(" ")[2] == "2" ): # Only Atoms of this type are considered (1= Hydrogen, 2=Oxygen) 
                X_POS = float(LINE.split(" ")[4])
                Y_POS = float(LINE.split(" ")[5])
                RAD_POS = (X_POS-ACHSE_X)*(X_POS-ACHSE_X)
                RAD_POS += (Y_POS-ACHSE_Y)*(Y_POS-ACHSE_Y)
                RAD_POS = math.sqrt(RAD_POS)
                XY_SCATTERPLOT_LIST.append(str(X_POS)+" "+str(Y_POS))
                #PARTIAL_AREA_NUMBER = int(math.floor(RAD_POS*RADIUS_SPLIT/RADIUS_MAX))
                PARTIAL_AREA_NUMBER = int(math.floor(RAD_POS*RADIUS_SPLIT/RADIUS))
                #if (RAD_POS > RADIUS_MAX):
                #    print RAD_POS
                #    continue
                ATOM_COUNTER[PARTIAL_AREA_NUMBER] += 1.0
                continue
            continue
        if (LINE[0:40] == "ITEM: ATOMS id mol type q x y z xu yu zu"):
            FROMHERE=True
            continue
        if ( LINE[0:25] == "ITEM: BOX BOUNDS pp pp pp" ):
#            print "Reading box bounds"
            XFROMHERE=True
            continue
        if ( XFROMHERE | YFROMHERE | ZFROMHERE):
            if ( XFROMHERE ):
#                print "Reading x-axis"
                BOX_X_LOW = float(LINE.split(" ")[0])
                BOX_X_UPP = float(LINE.split(" ")[1])
                XFROMHERE=False
                YFROMHERE=True
                continue
            if ( YFROMHERE ):
#                print "Reading y-axis"
                BOX_Y_LOW = float(LINE.split(" ")[0])
                BOX_Y_UPP = float(LINE.split(" ")[1])
                YFROMHERE=False
                ZFROMHERE=True
                continue
            if ( ZFROMHERE ):
#                print "Reading z-axis"
                BOX_Z_LOW = float(LINE.split(" ")[0])
                BOX_Z_UPP = float(LINE.split(" ")[1])
                ACHSE_X = (BOX_X_UPP+BOX_X_LOW)*0.5
                ACHSE_Y = (BOX_Y_UPP+BOX_Y_LOW)*0.5
                RADIUS_MAX = min((BOX_X_UPP-BOX_X_LOW), (BOX_Y_UPP-BOX_Y_LOW))*0.5 
                CYL_HEIGHT = (BOX_Z_UPP-BOX_Z_LOW)
                ZFROMHERE = False
                continue
POSF.close()
print "Done reading data."


#RADIUS = RADIUS_MAX
print "Found maximal RADIUS_MAX ", RADIUS_MAX
print "Found RADIUS ", RADIUS, " and splitting it ", RADIUS_SPLIT, " times."
RADIUS_SPLIT_PLOT = numpy.linspace(RADIUS/RADIUS_SPLIT, RADIUS, RADIUS_SPLIT)
print "Cylinder height: ", CYL_HEIGHT


# A_n-A_n-1 = pi*((R/Z)**2*((n)**2-(n-1)**2) = pi * (R/Z)**2 * (2*n-1)
# calculate atoms per cylinder shell with thickness RADIUS/RADIUS_SPLIT
for i in range(0, len(ATOM_COUNTER), 1):
   NUMBER_DENSITY[i]= ATOM_COUNTER[i]*((RADIUS_SPLIT/RADIUS)**2.0)/(3.1415*CYL_HEIGHT*(2.0*(i+1.0)-1.0))
ATOM_DENSITY = NUMBER_DENSITY*29.915


print "Writing data to file."
OUTFILE = "data/density_distribution-"+str(POSFILE.split("/")[-1])+".csv"
OUTF = open(OUTFILE, "w+")
OUTF.write("radius [Å] ; number density [Molecules/Å³] ; density [g/cm³]\n")
for i in range(0, len(ATOM_COUNTER), 1):
   OUTF.write(str(RADIUS_SPLIT_PLOT[i])+" ; "+str(NUMBER_DENSITY[i])+" ; "+str(ATOM_DENSITY[i])"\n")
OUTF.close()

