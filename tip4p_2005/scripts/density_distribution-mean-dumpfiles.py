#!/usr/bin/python3
# -*- coding: utf-8 -*-
## coding = utf-8

import sys
import math
import io
import numpy
#import matplotlib.pyplot as plt

"""
calculate RADIUS dependent density distribution for Oxygen atoms in cylinders-shells
POSFILE_LIST = file containing list of lammps dump files

the box dimensions are taken from the file header
ITEM: BOX BOUNDS pp pp pp
information in the lines following the header have to look like this:
ITEM: ATOMS id type x y z vx vy vz ix iy iz

H2O molecular density should be ~1/29.915 [Molecules/Å³] for density of 1 [kg/dm³]

RADIUS_SPLIT: in how many segments the RADIUS should be split
i.e. the thickness of the cylinder shells = RADIUS / RADIUS_SPLIT
"""

POSFILE_LIST = sys.argv[1]
RADIUS_WALL = float(sys.argv[2])
RADIUS_SPLIT = int(float(sys.argv[3]))
NUM_LINES = float(sys.argv[4])
ATOM_COUNTER = numpy.zeros(RADIUS_SPLIT)
ATOM_COUNTER_SQUARED = numpy.zeros(RADIUS_SPLIT)
ATOM_COUNTER_STDDEV = numpy.zeros(RADIUS_SPLIT)

print("Found file "+str(POSFILE_LIST))

# get box size and cylinder axis from lammps data file
# get oxygen atoms position (ID = 2) from lammps data file
# calculate distance from cylinder axis to oxygen atom
NUM_FILES = 0
RADIUS_MAX = 0.0
with open(POSFILE_LIST, 'r') as POSF_LIST:
    for LINE_POSF_LIST in POSF_LIST:
        LINE_POSF_LIST = LINE_POSF_LIST[:-1]
        XFROMHERE = False
        YFROMHERE = False
        ZFROMHERE = False
        FROMHERE = False
        CURRENT_ATOM_COUNTER = numpy.zeros(RADIUS_SPLIT)
        print("Reading data from file: "+str(LINE_POSF_LIST))
        with open(LINE_POSF_LIST, 'r') as POSF:
            NUM_FILES += 1
            POSFILE = str(POSF).split(".data")[0]+".data"
            for line in POSF:
                if FROMHERE:
                    # Only Atoms of this type are considered (1 = Hydrogen, 2 = Oxygen)
                    if line.split(" ")[2] == "2":
                        X_POS = float(line.split(" ")[4])
                        Y_POS = float(line.split(" ")[5])
                        RAD_POS = (X_POS-ACHSE_X)**2 + (Y_POS-ACHSE_Y)**2
                        RAD_POS = math.sqrt(RAD_POS)
                        PARTIAL_AREA_NUMBER = int(math.floor(RAD_POS*RADIUS_SPLIT/RADIUS_WALL))
    #                   PARTIAL_AREA_NUMBER = int(math.floor(RAD_POS*RADIUS_SPLIT/RADIUS_MAX))
                        CURRENT_ATOM_COUNTER[PARTIAL_AREA_NUMBER] += 1
                elif line[0:40] == "ITEM: ATOMS id mol type q x y z xu yu zu":
                    FROMHERE = True
                    continue
                elif line == "ITEM: BOX BOUNDS pp pp pp\n":
                    XFROMHERE = True
                    continue
                elif XFROMHERE | YFROMHERE | ZFROMHERE:
                    if XFROMHERE:
                        BOX_X_LOW = float(line.split(" ")[0])
                        BOX_X_UPP = float(line.split(" ")[1])
                        XFROMHERE = False
                        YFROMHERE = True
                        continue
                    elif YFROMHERE:
                        BOX_Y_LOW = float(line.split(" ")[0])
                        BOX_Y_UPP = float(line.split(" ")[1])
                        YFROMHERE = False
                        ZFROMHERE = True
                        continue
                    elif ZFROMHERE:
                        BOX_Z_LOW = float(line.split(" ")[0])
                        BOX_Z_UPP = float(line.split(" ")[1])
                        ACHSE_X = (BOX_X_UPP+BOX_X_LOW)*0.5
                        ACHSE_Y = (BOX_Y_UPP+BOX_Y_LOW)*0.5
                        RADIUS_MAX_BOX = min((BOX_X_UPP-BOX_X_LOW), (BOX_Y_UPP-BOX_Y_LOW))*0.5
                        RADIUS_MAX = max(RADIUS_MAX, RADIUS_MAX_BOX)
                        CYL_HEIGHT = (BOX_Z_UPP-BOX_Z_LOW)
                        ZFROMHERE = False
                        continue
        POSF.close()
        for i in range(0, RADIUS_SPLIT, 1):
            ATOM_COUNTER[i] += CURRENT_ATOM_COUNTER[i]
            ATOM_COUNTER_SQUARED[i] += CURRENT_ATOM_COUNTER[i]*CURRENT_ATOM_COUNTER[i]
POSF_LIST.close()
print("Done reading data.")
print("Number of files read: "+str(NUM_FILES))


# calculate arithmetic mean of ATOM_COUNTER and ATOM_COUNTER_SQUARED
ATOM_COUNTER = ATOM_COUNTER/float(NUM_FILES)
ATOM_COUNTER_SQUARED = ATOM_COUNTER_SQUARED/float(NUM_FILES)
# calculate STDDEV of ATOM_COUNTER
for i in range(0, RADIUS_SPLIT, 1):
    ATOM_COUNTER_STDDEV[i] = math.sqrt(ATOM_COUNTER_SQUARED[i]-ATOM_COUNTER[i]*ATOM_COUNTER[i])


# A_n-A_n-1 = pi*((R/Z)**2*((n)**2-(n-1)**2) = pi * (R/Z)**2 * (2*n-1)
# calculate atom density per cylinder shell with thickness RADIUS/RADIUS_SPLIT
NUMBER_DENSITY = numpy.zeros(RADIUS_SPLIT)
NUMBER_DENSITY_STDDEV = numpy.zeros(RADIUS_SPLIT)
ATOM_DENSITY = numpy.zeros(RADIUS_SPLIT)
ATOM_DENSITY_STDDEV = numpy.zeros(RADIUS_SPLIT)
for i in range(0, len(ATOM_COUNTER), 1):
    PARTIAL_AREA = ((RADIUS_SPLIT/RADIUS_WALL)**2.0)/(math.pi*CYL_HEIGHT*(2.0*(i+1.0)-1.0))
    NUMBER_DENSITY[i] = ATOM_COUNTER[i]*PARTIAL_AREA
    NUMBER_DENSITY_STDDEV[i] = ATOM_COUNTER_STDDEV[i]*PARTIAL_AREA
    ATOM_DENSITY[i] = NUMBER_DENSITY[i]*29.915
    ATOM_DENSITY_STDDEV[i] = NUMBER_DENSITY_STDDEV[i]*29.915


OUT_FILENAME = str(POSFILE_LIST).split("/")[-1]
OUT_FILENAME = "data/density_distribution-mean-"+OUT_FILENAME+".csv"
print "Writing data to file: ", OUT_FILENAME
with open(OUT_FILENAME, "w+") as OUTFILE:
    FIRST_LINE_OUTFILE = "partial_radius_no []"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+" ; mean_number [Molecules/cylinder_shell]"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+" ; mean_number_STDDEV [Molecules/cylinder_shell]"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+" ; partial_radius [Å]"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+" ; number_density [Molecules/Å³]"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+" ; number_density_STDDEV [Molecules/Å³]"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+" ; mean_density [g/cm³]"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+" ; mean_density_STDDEV [g/cm³]"
    FIRST_LINE_OUTFILE = FIRST_LINE_OUTFILE+"\n"
    OUTFILE.write(FIRST_LINE_OUTFILE)
    SECOND_LINE_OUTFILE = "0 ; 0.0 ; 0.0"
    SECOND_LINE_OUTFILE = SECOND_LINE_OUTFILE+" ; 0.0 ; 0.0 ; 0.0"
    SECOND_LINE_OUTFILE = SECOND_LINE_OUTFILE+"\n"
    OUTFILE.write(SECOND_LINE_OUTFILE)
    for i in range(0, RADIUS_SPLIT, 1):
        LINE_OUTFILE = str(float(i+1))
        LINE_OUTFILE = LINE_OUTFILE+" ; "+str(float(ATOM_COUNTER[i]))
        LINE_OUTFILE = LINE_OUTFILE+" ; "+str(float(ATOM_COUNTER_STDDEV[i]))
        PARTIAL_RADIUS = (i+1)*RADIUS_WALL/RADIUS_SPLIT
        LINE_OUTFILE = LINE_OUTFILE+" ; "+str(PARTIAL_RADIUS)
        LINE_OUTFILE = LINE_OUTFILE+" ; "+str(NUMBER_DENSITY[i])
        LINE_OUTFILE = LINE_OUTFILE+" ; "+str(NUMBER_DENSITY_STDDEV[i])
        LINE_OUTFILE = LINE_OUTFILE+" ; "+str(ATOM_DENSITY[i])
        LINE_OUTFILE = LINE_OUTFILE+" ; "+str(ATOM_DENSITY_STDDEV[i])
        LINE_OUTFILE = LINE_OUTFILE+"\n"
        OUTFILE.write(LINE_OUTFILE)
OUTFILE.close()
print "Done."

#RADIUS = RADIUS_WALL
#RADIUS = RADIUS_MAX
#RADIUS_SPLIT_PLOT = numpy.linspace(RADIUS/RADIUS_SPLIT, RADIUS, RADIUS_SPLIT)
#print("Found RADIUS "+str(RADIUS)+" and splitting it "+str(RADIUS_SPLIT)+" times.")
#print("Writing data to density-distribution-plot.")
#plotfilename = str("./data/"+POSFILE.split("/")[-1].split("[0-9]*.data")[0])
#plotfilename += ".dichteverteilung-mittelwert.svg"
#plt.title("radial density distribution")
#plt.xlabel("RADIUS [Å]")
#plt.ylabel("oxygen density [Atoms/Å³]")
#plt.plot(RADIUS_SPLIT_PLOT, ATOM_DENSITY, '.r')
##plt.plot(RADIUS_SPLIT_PLOT, ATOM_COUNTER, '.r')
# H2O molecular density should be ~1/30 [Molecules/Å³] for density of 1 [kg/dm³]
#plt.ylim((0.025,0.04))
#plt.savefig(plotfilename)
#print("Saved plot to: "+str(plotfilename))
