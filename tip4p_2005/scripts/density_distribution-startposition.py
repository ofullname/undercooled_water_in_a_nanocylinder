#!/usr/bin/python3
# coding=utf-8

import sys
import math
import io
import numpy
import matplotlib.pyplot as plt

"""
calculate RADIUS dependent density distribution for Oxygen atoms in cylinders-shells
POSFILE = lammps simulation init file
the box dimensions are taken from the file header
information in the lines following the header have to look like this:
id type charge x y z ix iy iz

H2O molecular density should be ~1/29.915 [Molecules/Å³] for density of 1 [kg/dm³]

as maximum RADIUS the smaller width of the box is used: RADIUS = min(box_x, box_y)
RADIUS_SPLIT: in how many segments the RADIUS should be split
i.e. the thickness of the cylinder shells = RADIUS / RADIUS_SPLIT
"""

POSFILE = sys.argv[1]
#RADIUS_SPLIT = 100
RADIUS_SPLIT = float(sys.argv[2])
NUMBER_DENSITY = numpy.zeros(int(RADIUS_SPLIT))
ATOM_DENSITY = numpy.zeros(int(RADIUS_SPLIT))
ATOM_COUNTER = numpy.zeros(int(RADIUS_SPLIT))
PARTIAL_RADIUS_ARRAY = numpy.zeros(int(RADIUS_SPLIT))
print("Found file "+str(POSFILE))


# get box size and cylinder axis from lammps data file
# get oxygen atoms position (ID=2) from lammps data file
# calculate distance from cylinder axis to oxygen atom
print("Reading data from file.")
XFROMHERE=False
YFROMHERE=False
ZFROMHERE=False
FROMHERE=False
LINE_NUMBER = 0
XY_SCATTERPLOT_LIST = []
with open(POSFILE, 'r') as F:
   for LINE in F:
      if ( FROMHERE & (len(LINE.split(" ")) == 10)):
         if ( LINE.split(" ")[2] == "2" ): # Only Atoms of this type are considered (1= Hydrogen, 2=Oxygen) 
            X_POS = float(LINE.split(" ")[4])
            Y_POS = float(LINE.split(" ")[5])
            RAD_POS = (X_POS-ACHSE_X)**2 + (Y_POS-ACHSE_Y)**2
            RAD_POS = math.sqrt(RAD_POS)
            if (RAD_POS >= RADIUS):
               continue
            XY_SCATTERPLOT_LIST.append(str(X_POS)+" "+str(Y_POS))
            PARTIAL_AREA_NUMBER = int(math.floor(RAD_POS*RADIUS_SPLIT/RADIUS))
            LINE_NUMBER+=1
            ATOM_COUNTER[PARTIAL_AREA_NUMBER] += 1.0
            continue
         elif (LINE == "Velocities\n"):
            break
         continue
      elif (LINE == "Atoms # full\n"):
         FROMHERE=True
      elif ( LINE == "1 angle types\n" ):
            XFROMHERE=True
      elif ( XFROMHERE | YFROMHERE | ZFROMHERE):
         if ( XFROMHERE & (len(LINE.split(" ")) == 4)):
            BOX_X_LOW = float(LINE.split(" ")[0])
            BOX_X_UPP = float(LINE.split(" ")[1])
            XFROMHERE=False
            YFROMHERE=True
            continue
         elif ( YFROMHERE ):
            BOX_Y_LOW = float(LINE.split(" ")[0])
            BOX_Y_UPP = float(LINE.split(" ")[1])
            YFROMHERE=False
            ZFROMHERE=True
            continue
         elif ( ZFROMHERE ):
            BOX_Z_LOW = float(LINE.split(" ")[0])
            BOX_Z_UPP = float(LINE.split(" ")[1])
            ACHSE_X = (BOX_X_UPP+BOX_X_LOW)*0.5
            ACHSE_Y = (BOX_Y_UPP+BOX_Y_LOW)*0.5
            CYL_HEIGHT = (BOX_Z_UPP-BOX_Z_LOW)
            ZFROMHERE = False
            RADIUS = min((BOX_X_UPP-BOX_X_LOW), (BOX_Y_UPP-BOX_Y_LOW))*0.5
            RADIUS_SPLIT_plot = numpy.linspace(RADIUS/RADIUS_SPLIT, RADIUS, RADIUS_SPLIT)
            RAD_SQUARE = RADIUS**2
            print("Found RADIUS "+str(RADIUS)+" and splitting it "+str(RADIUS_SPLIT)+" times.")
            continue
F.close()
print("Done reading data.")


# A_n-A_n-1 = pi*((R/Z)**2*((n)**2-(n-1)**2) = pi * (R/Z)**2 * (2*n-1)
# calculate atoms per cylinder shell with thickness RADIUS/RADIUS_SPLIT
for i in range(0, len(ATOM_COUNTER), 1):
   NUMBER_DENSITY[i]= ATOM_COUNTER[i]*((RADIUS_SPLIT/RADIUS)**2.0)/(3.1415*CYL_HEIGHT*(2.0*(i+1.0)-1.0))
ATOM_DENSITY = NUMBER_DENSITY*29.915

#print("Writing data to density-distribution-plot.")
#plotfilename=str("./data/"+POSFILE.split("/")[-1].split(".data")[0]+".dichteverteilung.svg")
#plt.title("radial density distribution")
#plt.xlabel("RADIUS [Å]")
#plt.ylabel("oxygen density [Atoms/Å³]")
#plt.plot(RADIUS_SPLIT_plot, ATOM_DENSITY, '.r')
##plt.plot(RADIUS_SPLIT_plot, ATOM_COUNTER, '.r')
#plt.ylim((0.025,0.04))
#plt.savefig(plotfilename)
#print("Saved plot to: "+str(plotfilename)) 

