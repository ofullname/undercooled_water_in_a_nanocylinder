#!/usr/bin/python3
# coding=utf-8

import sys
import math
import io

startfile = sys.argv[1]
infile = sys.argv[2]
outfile = sys.argv[3]
datafile = open(outfile, "w")


to_here = True
with open(startfile,'r') as f:
   for line in f:
      if len(line.rsplit()) == 4:
         if (line.rsplit()[2] == "xlo"):
            break
      else:
         datafile.write(line)

to_here = True
from_here = False
xboxfromhere = False
yboxfromhere = False
zboxfromhere = False
with open(infile,'r') as f:
   for line in f:
      if line == "ITEM: BOX BOUNDS pp pp pp\n":
         xboxfromhere = True
         continue
      elif xboxfromhere :
         datafile.write(line[:-1]+" xlo xhi\n")
         xboxfromhere=False
         yboxfromhere=True
         continue
      elif yboxfromhere :
         datafile.write(line[:-1]+" ylo yhi\n")
         yboxfromhere=False
         zboxfromhere=True
         continue
      elif zboxfromhere :
         datafile.write(line[:-1]+" zlo zhi\n\n")
         zboxfromhere=False
         break

fromhere = False
with open(startfile,'r') as f:
   for line in f:
      if line == "Masses\n":
         fromhere=True
      if fromhere:
         datafile.write(line)
      if line == "Atoms # full\n":
         datafile.write("\n")
         break

# write positions
from_here = False
with open(infile,'r') as f:
   for line in f:
      if from_here :
         if ( len(line.rsplit()) >= 11 ):
            helperino=""
            for word in line.rsplit()[0:3] :
               helperino+=str(word)+" "
            for word in line.rsplit()[3:7] :
               helperino+=str(float(word))+" "
            datafile.write(str(helperino)+"\n")
      elif ( line[0:40] == "ITEM: ATOMS id mol type q x y z xu yu zu" ):
         from_here = True

# write velocities
from_here = False
with open(infile,'r') as f:
   for line in f:
      if from_here :
         if ( len(line.rsplit()) >= 11 ):
            helperino=str(line.rsplit()[0])
            for word in line.rsplit()[7:10] :
               helperino+=" "+str(word)
            datafile.write(str(helperino)+"\n")
      elif ( line[0:40] == "ITEM: ATOMS id mol type q x y z xu yu zu" ):
         datafile.write("\nVelocities\n\n")
         from_here = True

# write bonds and angles
from_here = False
with open(startfile,'r') as f:
   for line in f:
      if from_here :
#         print(line)
#         print(str(line.rsplit()[0])+" 1 "+str(line.rsplit()[1:4]))
         datafile.write(str(line))
      elif line == "Bonds\n":
         datafile.write("\nBonds\n")
         from_here = True
#         print(line, fromhere)

