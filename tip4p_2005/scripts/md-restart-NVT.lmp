variable varDensity     equal 29.915*count(all)/(3*vol)
variable varSpecVol     equal 3*vol/count(all)
variable varVol         equal vol
variable varCellA       equal cella
variable varCellB       equal cellb
variable varCellC       equal cellc
variable varCellAlpha   equal cellalpha
variable varCellBeta    equal cellbeta
variable varCellGamma   equal cellgamma

#--------------------------------------------------

variable model_base        string ./lammps_models
variable model_pre_read    string ${model_base}/${model}_pre_read.lmp
variable model_post_read   string ${model_base}/${model}_post_read.lmp
variable model_post_int    string ${model_base}/${model}_post_int.lmp

#--------------------------------------------------

include ${model_pre_read}
read_dump ${infile} ${num_last_step} id mol type q x y z xu yu zu xs ys zs xsu ysu zsu ix iy iz vx vy vz box yes
include ${model_post_read}
variable number_atoms   equal count(all)

#--------------------------------------------------
# Integration

log data/NVT-number_atoms_${number_atoms}-temp_${t_start}_${t_stop}-run_${num_run}.log

dump CUSTOM all custom ${num_dump} trajectories/H2O-number_atoms_${number_atoms}-NVT-temp_${t_start}_${t_stop}-run_${num_run}.*.data id mol type q x y z xu yu zu xs ys zs xsu ysu zsu ix iy iz vx vy vz

fix INT all nvt temp ${t_start} ${t_stop} ${t_damp}
include ${model_post_int}

thermo_style custom step press pe ke etotal temp f_INT v_varDensity vol c_msd[1] c_msd[2] c_msd[3] c_msd[4]

thermo ${num_thermo}
run ${num_run}

#--------------------------------------------------

write_data trajectories/H2O-number_atoms_${number_atoms}-NVT-temp_${t_start}_${t_stop}-run_${num_run}.simulation_end_state.data
write_data starting_positions/H2O-number_atoms_${number_atoms}-NVT-temp_${t_start}_${t_stop}-run_${num_run}.simulation_end_state.data
write_data data/H2O-number_atoms_${number_atoms}-NVT-temp_${t_start}_${t_stop}-run_${num_run}.simulation_end_state.data

