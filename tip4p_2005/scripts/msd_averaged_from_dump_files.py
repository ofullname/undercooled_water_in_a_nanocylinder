#!/bin/pyhton3
# coding=utf-8

"""
Write averaged MSD from oxygen atoms to file.
One line per INIT_NR.
Each column corresponding to an atoms coordinate:
t[ps] msd_x[Å²] msd_x_stdev[Å²] msd_y[Å²] msd_y_stdev[Å²] msd_z[Å²] msd_z_stdev[Å²]

The name of the created file contains information about the simulation:
msd_average_from_dumpfiles-<infile>-<number_molecules>-<number_INIT_NRs>-<box_parametes>.csv

DUMPFILE_LIST = file containing list of lammps dump files

information in the lines following the header have to look like this:
ITEM: ATOMS  id mol type q x y z xu yu zu xs ys zs xsu ysu zsu <optional other variables>
"""

import sys
#import os
import math
import numpy


# --- PART 1: Reading in variables and defining name of datafile ---

DUMPFILE_LIST = sys.argv[1]
NUMBER_MOLECULES = int(float(sys.argv[2]))      # number of molecules used in simulation
SIMULATION_TIMESTEP = float(sys.argv[3])        # timestep from one position file to the next in fs
SIMULATION_TIMESTEP /= 1000.0                   # use timestep in ps

print "Found file ", str(DUMPFILE_LIST)
print "Number of molecules ", str(NUMBER_MOLECULES)
print "Simulation time step between files ", str(SIMULATION_TIMESTEP), " ps."

MSD_OUTFILE_NAME = "data/msd_averaged_from_dumpfiles"
MSD_OUTFILE_NAME += "-"+str(DUMPFILE_LIST.split("/")[-1])
MSD_OUTFILE_NAME += ".csv"


# --- PART 2: Reading list of dumpfiles ---

print "Reading filelist to get total number of files."
TOTAL_NUM_FILES = 0
with open(DUMPFILE_LIST) as POSF_LIST:
    for LINE_DUMPFILE_LIST in POSF_LIST:
        TOTAL_NUM_FILES += 1
POSF_LIST.close()
print "Total number of lines is: ", TOTAL_NUM_FILES


# --- PART 3: Reading list of dumpfiles and get id of atoms in cylinder shells ---

MSD_AVG_NUM = 10
# Array containing starting positions at time step n
INIT_U_POS = numpy.zeros((MSD_AVG_NUM, 3*NUMBER_MOLECULES+1, 3))
U_POS_T = numpy.zeros(3)
CURR_FILE_NR = 0
print "Reading data from dump files to get starting positions."
with open(DUMPFILE_LIST, 'r') as POSF_LIST:
    for LINE_DUMPFILE_LIST in POSF_LIST:
        # [:-1] to ignore the "\n" written to the file at the end of read each line
        LINE_DUMPFILE_LIST = LINE_DUMPFILE_LIST[:-1]
        CURR_FILE_NR += 1
        n = CURR_FILE_NR-1
        if CURR_FILE_NR > MSD_AVG_NUM:
            POSF_LIST.close()
            print "Done reading starting positions."
            break
        print "Processing file: ", LINE_DUMPFILE_LIST
        with open(LINE_DUMPFILE_LIST, 'r') as DUMPF:
            CURR_NUM_MOL_READ = 0
            XFROMHERE = False
            YFROMHERE = False
            ZFROMHERE = False
            FROMHERE = False
            for LINE in DUMPF:
                if FROMHERE:
                    SPLITLINE = LINE.split(" ")
                    ATOM_TYPE = SPLITLINE[2]
                    # Only Atoms of this type are considered (1=Hydrogen, 2=Oxygen)
                    if ATOM_TYPE == "2":
                        CURR_NUM_MOL_READ += 1
                        SPLITLINE = LINE.split(" ")
                        ATOM_ID = int(float(SPLITLINE[0]))
                        for i in range(0, 3):
                            U_POS_T[i] = float(SPLITLINE[7+i])
                            INIT_U_POS[(n, ATOM_ID, i)] = U_POS_T[i]
                        if CURR_NUM_MOL_READ == NUMBER_MOLECULES:
                            print "Done reading positions for file nr.: ", CURR_FILE_NR
                            break
                #                               0   1    2 3 4 5 6  7  8  9 10 11 12 13 14 15
                elif LINE[:61] == "ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu":
                    FROMHERE = True
                elif LINE == "ITEM: BOX BOUNDS pp pp pp\n":
                    XFROMHERE = True
                    continue
                if XFROMHERE | YFROMHERE | ZFROMHERE:
                    if XFROMHERE:
#                        print "Getting box parameters from file header nr ", CURR_FILE_NR
                        box_x_low = float(LINE.split(" ")[0])
                        box_x_upp = float(LINE.split(" ")[1])
                        if CURR_FILE_NR == 1:
                            BOX_X_LOW_INITIAL = box_x_low
                            BOX_X_UPP_INITIAL = box_x_upp
                        XFROMHERE = False
                        YFROMHERE = True
                        continue
                    if YFROMHERE:
                        box_y_low = float(LINE.split(" ")[0])
                        box_y_upp = float(LINE.split(" ")[1])
                        if CURR_FILE_NR == 1:
                            BOX_Y_LOW_INITIAL = box_y_low
                            BOX_Y_UPP_INITIAL = box_y_upp
                        YFROMHERE = False
                        ZFROMHERE = True
                        continue
                    if ZFROMHERE:
                        box_z_low = float(LINE.split(" ")[0])
                        box_z_upp = float(LINE.split(" ")[1])
                        if CURR_FILE_NR == 1:
                            BOX_Z_LOW_INITIAL = box_z_low
                            BOX_Z_UPP_INITIAL = box_z_upp
                        ZFROMHERE = False
                        continue
        DUMPF.close()
POSF_LIST.close()
print "Done reading start positions."


# --- PART 4: Reading all dumpfiles and calculating MSD_n ---

# Mean Square Displacement and its variance are computed simultaneously for each time step n
MSD_T = numpy.zeros((MSD_AVG_NUM, TOTAL_NUM_FILES - MSD_AVG_NUM, 3))
MSD_T_VAR = numpy.zeros((MSD_AVG_NUM, TOTAL_NUM_FILES - MSD_AVG_NUM, 3))
U_POS_T = numpy.zeros((3*NUMBER_MOLECULES+1, 3))
CURR_FILE_NR = 0
print "Reading data from dump files and computing MSD."
with open(DUMPFILE_LIST, 'r') as POSF_LIST:
    for LINE_DUMPFILE_LIST in POSF_LIST:
        # [:-1] to ignore the "\n" written to the file at the end of read each line
        LINE_DUMPFILE_LIST = LINE_DUMPFILE_LIST[:-1]
        CURR_FILE_NR += 1
        with open(LINE_DUMPFILE_LIST, 'r') as DUMPF:
            print "Processing file: ", LINE_DUMPFILE_LIST
            # Atom positions of current time step
            CURR_MOL_NUM = 0
            FROMHERE = False
            for LINE in DUMPF:
                if FROMHERE:
                    SPLITLINE = LINE.split(" ")
                    ATOM_TYPE = SPLITLINE[2]
                    # Only Atoms of this type are considered (1=Hydrogen, 2=Oxygen)
                    if ATOM_TYPE == "2":
                        CURR_MOL_NUM += 1
                        ATOM_ID = int(float(SPLITLINE[0]))
                        # Write unwrapped atom positions of current time step to array
                        for i in range(0, 3):
                            U_POS_T[(ATOM_ID, i)] = float(SPLITLINE[7+i])
                        # After reading all positions
                        if CURR_MOL_NUM == NUMBER_MOLECULES:
                            # Compute MSD_n for current time step
                            for n in range(0, MSD_AVG_NUM):
                                if (n + 1) > CURR_FILE_NR:
                                    break
                                if (MSD_AVG_NUM + CURR_FILE_NR - (n + 1)) >= TOTAL_NUM_FILES:
                                    continue
                                T = CURR_FILE_NR - n - 1
                                for mol in range(0, NUMBER_MOLECULES):
                                    for j in range(0, 3):
                                        POS_DIFF = U_POS_T[3*mol+1, j] - INIT_U_POS[n, 3*mol+1, j]
                                        DIFF_SQUARED = POS_DIFF*POS_DIFF/float(NUMBER_MOLECULES)
                                        MSD_T[(n, T, j)] += DIFF_SQUARED
                                        MSD_VAR_HELP = DIFF_SQUARED*DIFF_SQUARED
                                        MSD_VAR_HELP = MSD_VAR_HELP*float(NUMBER_MOLECULES)
                                        MSD_VAR_HELP -= DIFF_SQUARED*DIFF_SQUARED
                                        MSD_VAR_HELP = MSD_VAR_HELP/float(NUMBER_MOLECULES)
                                        MSD_T_VAR[(n, T, j)] += MSD_VAR_HELP
                if LINE[:61] == "ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu":
                    FROMHERE = True
#                    print "Reading data from file."
                    continue
        DUMPF.close()
POSF_LIST.close()
print "Done reading data."


# --- PART 5: Write MSD to file ---

print "Writing data to: ", MSD_OUTFILE_NAME
FIRSTLINE = ""
FIRSTLINE += "t[ps]"
#for n in range(0, MSD_AVG_NUM):
#    FIRSTLINE += " ; msd_"+str(n)+"_x[Å²]"
#    FIRSTLINE += " ; msd_"+str(n)+"_x_stdev[Å²]"
#    FIRSTLINE += " ; msd_"+str(n)+"_y[Å²]"
#    FIRSTLINE += " ; msd_"+str(n)+"_y_stdev[Å²]"
#    FIRSTLINE += " ; msd_"+str(n)+"_z[Å²]"
#    FIRSTLINE += " ; msd_"+str(n)+"_z_stdev[Å²]"
FIRSTLINE += " ; MSD_x_averaged[Å²]"
FIRSTLINE += " ; MSD_x_averaged_stdev[Å²]"
FIRSTLINE += " ; MSD_y_averaged[Å²]"
FIRSTLINE += " ; MSD_y_averaged_stdev[Å²]"
FIRSTLINE += " ; MSD_z_averaged[Å²]"
FIRSTLINE += " ; MSD_z_averaged_stdev[Å²]"
FIRSTLINE += "\n"
MSD_OUTFILE = open(MSD_OUTFILE_NAME, "w")
MSD_OUTFILE.write(FIRSTLINE)
MSD_OUTFILE.close()

for t in range(0, TOTAL_NUM_FILES - MSD_AVG_NUM):
    LINE_INIT_NR = t*SIMULATION_TIMESTEP
    HELPSTRING = str(LINE_INIT_NR)
    MSD_AVERAGE = numpy.zeros(3)
    STDERR_AVG = numpy.zeros(3)
    for n in range(0, MSD_AVG_NUM):
        for i in range(0, 3):
            MSD_AVERAGE[i] += MSD_T[(n, t, i)]/float(MSD_AVG_NUM)
            MSD_STDERR = math.sqrt(MSD_T_VAR[(n, t, i)])
            STDERR_AVG[i] += MSD_STDERR*MSD_STDERR
#            HELPSTRING += " ; "+str(MSD_T[(n, t, i)])
#            HELPSTRING += " ; "+str(MSD_STDERR)
    for i in range(0, 3):
        STDERR_AVG[i] = math.sqrt(STDERR_AVG[i])/float(MSD_AVG_NUM)
        HELPSTRING += " ; "+str(MSD_AVERAGE[i])
        HELPSTRING += " ; "+str(STDERR_AVG[i])
    HELPSTRING += "\n"
#    print HELPSTRING
    MSD_OUTFILE = open(MSD_OUTFILE_NAME, "a")
    MSD_OUTFILE.write(HELPSTRING)
    MSD_OUTFILE.close()
print "Done writing data to outfile."
