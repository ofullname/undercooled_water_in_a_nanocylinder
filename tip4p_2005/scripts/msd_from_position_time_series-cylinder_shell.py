#!/bin/pyhton3
# coding=utf-8

"""
Read atom positions from time series file and calculate mean square displacement.
One line per timestep.
Each column corresponding to an atoms coordinate:
t[ps] sd_x_atom_1[Å] sd_y_atom_1[Å] sd_z_atom_1[Å] sd_x_atom_2[Å] sd_y_atom_2[Å] sd_z_atom_2[Å] ...

The name of the created file contains information about the simulation:
msd-<infile>-<number_molecules>-<number_timesteps>-<box_parametes>.csv

POSFILE_LIST = file containing list of lammps dump files
"""

import sys
import math
import io
import numpy

POSFILE = sys.argv[1]
print("Found file "+str(POSFILE))

OUTFILE_NAME = ".data/msd-"
OUTFILE_NAME += str(POSFILE_LIST)+".csv"
OUTFILE = open(OUTFILE_NAME_TMP, "w")
FIRSTLINE = "t[ps]"

for i in range(1, NUMBER_MOLECULES+1):
    FIRSTLINE += " ; x_atom_"+str(i)+"[Å]"
    FIRSTLINE += " ; y_atom_"+str(i)+"[Å]"
    FIRSTLINE += " ; z_atom_"+str(i)+"[Å]"
FIRSTLINE += "\n"
OUTFILE.write(FIRSTLINE)
OUTFILE.close()

NUM_FILES = 0
RADIUS_MAX = 0.0
ATOM_ID_DICT = {}
POS_DICT = {}

print("Reading data from file and writing it to position time series file.")
with open(POSFILE_LIST, 'r') as POSF_LIST:
    for LINE_POSF_LIST in POSF_LIST:
        # [:-1] to ignore the "\n" written to the file at the end of each LINE
        LINE_POSF_LIST = LINE_POSF_LIST[:-1]
        print("Reading data from file: "+str(LINE_POSF_LIST))
        with open(LINE_POSF_LIST, 'r') as POSF:
            POSFILE = str(POSF)
            XFROMHERE = False
            YFROMHERE = False
            ZFROMHERE = False
            FROMHERE = False
            NUM_FILES += 1
            ATOM_INSIDE_NR = 0
            for LINE in POSF:
                if FROMHERE:
                    # Only Atoms of this type are considered (1= Hydrogen, 2=Oxygen)
                    if LINE.split(" ")[2] == "2":
                        ATOM_ID = int(float(LINE.split(" ")[0]))
                        ATOM_INSIDE_NR += 1
                        if NUM_FILES == 1:
                            ATOM_ID_DICT.update({ATOM_ID:ATOM_INSIDE_NR})
                            POS_NR = ATOM_ID_DICT[ATOM_ID]
                        XU_POS = float(LINE.split(" ")[7])
                        YU_POS = float(LINE.split(" ")[8])
                        ZU_POS = float(LINE.split(" ")[9])
                        HELPSTRING = " ; "+str(XU_POS)
                        HELPSTRING += " ; "+str(YU_POS)
                        HELPSTRING += " ; "+str(ZU_POS)
                        POS_NR = ATOM_ID_DICT[ATOM_ID]
                        POS_DICT.update({POS_NR:HELPSTRING})
                        if ATOM_INSIDE_NR == NUMBER_MOLECULES:
                            LINE_TIMESTEP = str(float(float(NUM_FILES-1)*SIMULATION_TIMESTEP))
                            HELPSTRING = LINE_TIMESTEP
                            for i in range(1, NUMBER_MOLECULES+1):
                                HELPSTRING += POS_DICT[i]
                            HELPSTRING += "\n"
                            OUTFILE = open(OUTFILE_NAME_TMP, "a")
                            OUTFILE.write(HELPSTRING)
                            OUTFILE.close()
                        continue
                    continue
                elif LINE[:61] == "ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu":
                    FROMHERE = True
                    continue
                elif LINE == "ITEM: BOX BOUNDS pp pp pp\n":
                    XFROMHERE = True
                    continue
                elif XFROMHERE | YFROMHERE | ZFROMHERE:
                    if XFROMHERE:
                        box_x_low = float(LINE.split(" ")[0])
                        box_x_upp = float(LINE.split(" ")[1])
                        if NUM_FILES == 1:
                            BOX_X_LOW_INITIAL = box_x_low
                            BOX_X_UPP_INITIAL = box_x_upp
                            CYL_AXIS_X_INITIAL = (BOX_X_LOW_INITIAL+BOX_X_UPP_INITIAL)*0.5
                        XFROMHERE = False
                        YFROMHERE = True
                        continue
                    elif YFROMHERE:
                        box_y_low = float(LINE.split(" ")[0])
                        box_y_upp = float(LINE.split(" ")[1])
                        if NUM_FILES == 1:
                            BOX_Y_LOW_INITIAL = box_y_low
                            BOX_Y_UPP_INITIAL = box_y_upp
                            CYL_AXIS_Y_INITIAL = (BOX_Y_LOW_INITIAL+BOX_Y_UPP_INITIAL)*0.5
                        YFROMHERE = False
                        ZFROMHERE = True
                        continue
                    elif ZFROMHERE:
                        box_z_low = float(LINE.split(" ")[0])
                        box_z_upp = float(LINE.split(" ")[1])
                        if NUM_FILES == 1:
                            BOX_Z_LOW_INITIAL = box_z_low
                            BOX_Z_UPP_INITIAL = box_z_upp
                            CYL_INITIAL_HEIGHT = BOX_Z_UPP_INITIAL-BOX_Z_LOW_INITIAL
                        ZFROMHERE = False
                        continue
        POSF.close()
POSF_LIST.close()
print "Cylinder axis coordinates: ", CYL_AXIS_X_INITIAL, CYL_AXIS_Y_INITIAL
print("Done reading data.")

# The name of the created file contains information about the simulation:
# position_timeseries-<infile>-<number_molecules>-<number_timesteps>-<box_parametes>.csv
print("Renaming position timeseries file.")
OUTFILE_NAME = ".data/position_timeseries"
OUTFILE_NAME += "-"+str(POSFILE_LIST)
OUTFILE_NAME += "-number_molecules_"+str(NUMBER_MOLECULES)
OUTFILE_NAME += "-number_timesteps_"+str(NUM_FILES)
OUTFILE_NAME += "-box_x_"+str(BOX_X_LOW_INITIAL)+"_"+str(BOX_X_UPP_INITIAL)
OUTFILE_NAME += "-box_y_"+str(BOX_Y_LOW_INITIAL)+"_"+str(BOX_Y_UPP_INITIAL)
OUTFILE_NAME += ".csv"
os.rename(OUTFILE_NAME_TMP, OUTFILE_NAME)
