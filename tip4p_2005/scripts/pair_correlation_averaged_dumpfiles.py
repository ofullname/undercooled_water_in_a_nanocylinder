#!/bin/pyhton3
# coding=utf-8

"""
Write pair correlation function from oxygen atoms to file.

The name of the created file contains information about the simulation:
pair_correlation_average_from_dumpfiles-<infile>-<number_molecules>-<box_parametes>.csv

DUMPFILE_LIST = file containing list of lammps dump files

information in the lines following the header have to look like this:
ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu <optional other variables>
"""

import sys
#import os
import math
import numpy


# --- PART 1: Reading in variables and defining name of datafile ---

FILELIST = sys.argv[1]
NUM_MOLECULES = int(float(sys.argv[2]))
RADIUS_WALL = float(sys.argv[3])
# get MACRO_DENSITY from corresponding lammps logfile!
MACRO_DENSITY = float(sys.argv[4])
NUM_FILES = float(sys.argv[5])
NUM_ATOMS = 3*NUM_MOLECULES
RADIUS_SPLIT = 100
RADIUS_FROM = 0
RADIUS_TO = 0.2*RADIUS_SPLIT
print "Found file ", str(FILELIST)
print "Number molecules ", str(NUM_MOLECULES)
print "Radius wall ", str(RADIUS_WALL), "[Å]"
print "Macroscopic density ", str(MACRO_DENSITY), "[molecules/Å³]"
print "Number files to process ", str(NUM_FILES)


# --- PART 2: Reading position files from filelist ---

print "Reading lines from dump file list: ", str(FILELIST)
FULL_OXY_W_POS = numpy.zeros((int(NUM_FILES), int(NUM_MOLECULES), 3))
FULL_CENTRAL_POS = numpy.zeros((int(NUM_FILES), int(NUM_MOLECULES)))
NUM_CENTRAL_ATOMS = numpy.zeros(int(NUM_FILES))
FULL_AXIS_POS = numpy.zeros((int(NUM_FILES), 2))
#SPHERE_MAX_RADIUS = RADIUS_WALL*(0.95-(RADIUS_TO/RADIUS_SPLIT))
SPHERE_MAX_RADIUS = 20.0
print "Maximum radius of sphere ", str(SPHERE_MAX_RADIUS)
SPHERE_SPLIT = int(math.ceil(SPHERE_MAX_RADIUS/0.1))
W_POS = numpy.zeros(3)
U_POS = numpy.zeros(3)
CURR_FILE=-1
with open(FILELIST, 'r') as FLIST:
    print "Reading data from dump files to get starting positions."
    for LISTLINE in FLIST:
        LISTLINE = LISTLINE[:-1]
        CENTRAL_POS_IDS = []
        FULL_OXYGEN_IDS = []
        CURR_FILE += 1
        CURR_LINE = 0
        # Set the axis value here, otherwise the linter would not be amused
        CYL_AXIS_X_INITIAL = 0.0
        CYL_AXIS_Y_INITIAL = 0.0
        print "Processing file: ", LISTLINE
        print "File number ", CURR_FILE, " out of ", int(NUM_FILES)
        with open(LISTLINE, 'r') as DUMPF:
            for LINE in DUMPF:
                CURR_LINE += 1
                if CURR_LINE > 9:
                    #print "At line nr ", CURR_LINE
                    SPLITLINE = LINE.split(" ")
                    ATOM_TYPE = SPLITLINE[2]
                    #print ATOM_TYPE
                    # Only Atoms of this type are considered (1=Hydrogen, 2=Oxygen)
                    if ATOM_TYPE == "2":
                        #print "Found oxygen atom"
                        SPLITLINE = LINE.split(" ")
                        ATOM_ID = int(float(SPLITLINE[0]))
                        # Oxygen IDs for water in dumpfiles are multiples of 3 plus 1
                        OXY_NUM = int((ATOM_ID-1)/3)
                        for i in range(0, 3):
                            W_POS[i] = float(SPLITLINE[4+i])
                            U_POS[i] = float(SPLITLINE[7+i])
                            #U_POS[i] = float(SPLITLINE[13+i])
                            FULL_OXY_W_POS[int(CURR_FILE), OXY_NUM, i] = W_POS[i]
                        # get distance from oxygen to cylinder axis
                        DIST_AXIS_X = FULL_OXY_W_POS[int(CURR_FILE), OXY_NUM, 0] - FULL_AXIS_POS[int(CURR_FILE), 0]
                        DIST_AXIS_X = DIST_AXIS_X*DIST_AXIS_X
                        DIST_AXIS_Y = FULL_OXY_W_POS[int(CURR_FILE), OXY_NUM, 1] - FULL_AXIS_POS[int(CURR_FILE), 1]
                        DIST_AXIS_Y = DIST_AXIS_Y*DIST_AXIS_Y
                        DIST_AXIS = math.sqrt(DIST_AXIS_X+DIST_AXIS_Y)
                        DIST_NUMBER = DIST_AXIS*RADIUS_SPLIT/RADIUS_WALL
                        DIST_FLOOR = SPHERE_MAX_RADIUS
                        DIST_CEIL = BOX_Z_UPP_INITIAL-SPHERE_MAX_RADIUS
                        # get id of atoms initailly positioned in cylinder shell
                        # write array mask of atoms initailly positioned in cylinder shell
                        if (DIST_NUMBER < RADIUS_TO):
                            # since considering spheres of radius SPHERE_MAX_RADIUS
                            # it is necessary to consider upper and lower cylinder boundaries
                            #print str(DIST_NUMBER)
                            if (W_POS[2] > DIST_FLOOR) & (W_POS[2] < DIST_CEIL):
                                FULL_CENTRAL_POS[int(CURR_FILE), OXY_NUM] = 1.0
                                NUM_CENTRAL_ATOMS[int(CURR_FILE)] += 1.0
                if CURR_LINE == 6:
                    print "Getting box parameters from file header nr ", CURR_FILE
                    box_x_low = float(LINE.split(" ")[0])
                    box_x_upp = float(LINE.split(" ")[1])
        #            if CURR_NUM_FILES == 1:
                    BOX_X_LOW_INITIAL = box_x_low
                    BOX_X_UPP_INITIAL = box_x_upp
                    FULL_AXIS_POS[int(CURR_FILE), 0] = (BOX_X_UPP_INITIAL-BOX_X_LOW_INITIAL)*0.5
                if CURR_LINE == 7:
                    box_y_low = float(LINE.split(" ")[0])
                    box_y_upp = float(LINE.split(" ")[1])
        #            if CURR_NUM_FILES == 1:
                    BOX_Y_LOW_INITIAL = box_y_low
                    BOX_Y_UPP_INITIAL = box_y_upp
                    FULL_AXIS_POS[int(CURR_FILE), 1] = (BOX_Y_LOW_INITIAL+BOX_Y_UPP_INITIAL)*0.5
                if CURR_LINE == 8:
                    box_z_low = float(LINE.split(" ")[0])
                    box_z_upp = float(LINE.split(" ")[1])
        #           if CURR_NUM_FILES == 1:
                    BOX_Z_LOW_INITIAL = box_z_low
                    BOX_Z_UPP_INITIAL = box_z_upp
        print "Done reading data from file: ", LISTLINE
        print "Central molecules for this time step: ", NUM_CENTRAL_ATOMS[int(CURR_FILE)], "\n"
        DUMPF.close()
FLIST.close()
print "Done reading data from position files."


# --- PART 3: computing pair correlation function from position array and central positions list ---

print "Computing pair correlation function."
# consider the distance from initial positions (near cylinder axis) to
# the cylinders lateral surface. If RADIUS_WALL would be the max radius
# for the sphere, then we would look at a volume outside of the cylinder,
# leading to a systematical error in computing the pair correlation.
# SPHERE_SHELL_COUNTER_SUM[i] = total number of atoms in sphere shell i
SPHERE_SHELL_COUNTER_SUM = numpy.zeros(SPHERE_SPLIT)
# Compute distance of atoms to central positioned ones
# +1 to corresponding cylinder shell, divided by current number of cylinders
# then normalize it with total number of files read
print NUM_CENTRAL_ATOMS
NUM_NON_ZERO_CYLS = 0
for T_STEP in range(0, int(NUM_FILES)):
    print "At file number ", int(T_STEP), " out of ", int(NUM_FILES)
    NUMBER_SPHERES = NUM_CENTRAL_ATOMS[T_STEP]
    if NUMBER_SPHERES == 0:
        print "Skipping number ", int(T_STEP)
        continue
    NUM_NON_ZERO_CYLS += 1
    for INSIDE_ID in range(0, NUM_MOLECULES):
        # only consider distance to molecules around cylinder axis
        if FULL_CENTRAL_POS[T_STEP, INSIDE_ID] == 0:
            continue
        #print "At atom ", INSIDE_ID, " out of ", NUM_MOLECULES
        for ATOM_ID in range(0, NUM_MOLECULES):
            # an atom has no distance to itself
            if ATOM_ID == INSIDE_ID:
                continue
            FULL_DIST = 0.0
            for i in range(0, 3):
                DIST = FULL_OXY_W_POS[T_STEP, ATOM_ID, i] - FULL_OXY_W_POS[T_STEP, INSIDE_ID, i]
                FULL_DIST += DIST*DIST
            FULL_DIST = math.sqrt(FULL_DIST)
            if FULL_DIST <= SPHERE_MAX_RADIUS:
                DIST_NUMBER = int(math.floor(FULL_DIST*SPHERE_SPLIT/SPHERE_MAX_RADIUS))
                SPHERE_SHELL_COUNTER_SUM[DIST_NUMBER] += 1.0/NUMBER_SPHERES
print "Done counting atoms in sphere shells."

# sum up atoms (in all spheres) in shell j divided by total number of atoms in sphere i
# to get number density of atoms in shell j, then divide it by number of spheres
# MEAN_MOLECULES_PER_SPHERE_SHELL[j] = mean number density in shell j
MEAN_MOLECULES_PER_SPHERE_SHELL = numpy.zeros(SPHERE_SPLIT)
MEAN_MOLECULES_PER_SPHERE_SHELL = SPHERE_SHELL_COUNTER_SUM/NUM_NON_ZERO_CYLS
print "Done computing pair correlation function."

# --- PART 4: Write pair correlation data to file ---

print "Writing data to file."
FIRSTLINE = "r [Å]"
FIRSTLINE += " ; g(r)"
FIRSTLINE += " ; molecules in shell n [# molecules]"
for i in range(0, len(CENTRAL_POS_IDS)):
    FIRSTLINE += " ; mol. around oxygen "+str(i)+" [# molecules]"
FIRSTLINE += "\n"
PAIR_CORR_OUTFILE_NAME = "data/pair_correlation_averaged_from_dumpfile"
PAIR_CORR_OUTFILE_NAME += "-from_"+str(RADIUS_FROM)
PAIR_CORR_OUTFILE_NAME += "_to_"+str(RADIUS_TO)
PAIR_CORR_OUTFILE_NAME += "-"+str(FILELIST.split("/")[-1])
PAIR_CORR_OUTFILE_NAME = PAIR_CORR_OUTFILE_NAME.split(".data")[0]
PAIR_CORR_OUTFILE_NAME += ".csv"

PAIR_CORR_OUTFILE = open(PAIR_CORR_OUTFILE_NAME, "w")
PAIR_CORR_OUTFILE.write(FIRSTLINE)
D_R = SPHERE_MAX_RADIUS/SPHERE_SPLIT
# The mean volume of one water molecule is 1/29.915 1/Å³
PRE_FACTOR = MACRO_DENSITY/29.915*4.0*math.pi*D_R
for R_i in range(0, SPHERE_SPLIT):
    SPHERE_SHELL = (R_i+1)*D_R
    DIVISOR = PRE_FACTOR*SPHERE_SHELL*SPHERE_SHELL
    MOLECULES_PER_SHELL_VOLUME = MEAN_MOLECULES_PER_SPHERE_SHELL[R_i]/DIVISOR
    HELPSTRING = str(SPHERE_SHELL)
    HELPSTRING += " ; "+str(MOLECULES_PER_SHELL_VOLUME)+" "
    HELPSTRING += " ; "+str(SPHERE_SHELL_COUNTER_SUM[R_i])+" "
    HELPSTRING += "\n"
#    print HELPSTRING
    PAIR_CORR_OUTFILE.write(HELPSTRING)
PAIR_CORR_OUTFILE.close()
print "Done writing data to file: ", PAIR_CORR_OUTFILE_NAME
