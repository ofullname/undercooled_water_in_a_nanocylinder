#!/bin/pyhton3
# coding=utf-8

"""
Write pair correlation function from oxygen atoms to file.

The name of the created file contains information about the simulation:
pair_correlation_average_from_dumpfiles-<infile>-<number_molecules>-<box_parametes>.csv

DUMPFILE_LIST = file containing list of lammps dump files

information in the lines following the header have to look like this:
ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu <optional other variables>
"""

import sys
#import os
import math
import numpy


# --- PART 1: Reading in variables and defining name of datafile ---

DUMPFILE = sys.argv[1]
NUM_MOLECULES = int(float(sys.argv[2]))
RADIUS_WALL = float(sys.argv[3])
# get MACRO_DENSITY from corresponding lammps logfile!
MACRO_DENSITY = float(sys.argv[4])
NUM_ATOMS = 3*NUM_MOLECULES
RADIUS_SPLIT = 50
RADIUS_FROM = 0
RADIUS_TO = 5
print "Found file ", str(DUMPFILE)
print "Number molecules ", str(NUM_MOLECULES)
print "Radius wall ", str(RADIUS_WALL), "[Å]"
print "Macroscopic density ", str(MACRO_DENSITY), " [molecules/Å³]"


# --- PART 2: Reading positions from dumpfile ---

# Array containing starting positions inside cylinder shell at time step n
FULL_U_POS = numpy.zeros((NUM_ATOMS, 3))
OXY_W_U_POS = numpy.zeros((NUM_MOLECULES, 3))
CENTRAL_POS_IDS = []
FULL_OXYGEN_IDS = []
W_POS = numpy.zeros(3)
U_POS = numpy.zeros(3)
SPHERE_MAX_RADIUS = RADIUS_WALL*(0.95-RADIUS_TO/RADIUS_SPLIT)
SPHERE_SPLIT = 100
# Set the axis value here, otherwise the linter would not be amused
#CYL_AXIS_X_INITIAL = 0.0
#CYL_AXIS_Y_INITIAL = 0.0
print "Reading data from dump file to get starting positions."
print "Processing file: ", DUMPFILE
with open(DUMPFILE, 'r') as DUMPF:
    XFROMHERE = False
    YFROMHERE = False
    ZFROMHERE = False
    FROMHERE = False
    for LINE in DUMPF:
        if FROMHERE:
            SPLITLINE = LINE.split(" ")
            ATOM_TYPE = SPLITLINE[2]
            # Only Atoms of this type are considered (1=Hydrogen, 2=Oxygen)
            if ATOM_TYPE == "2":
                SPLITLINE = LINE.split(" ")
                ATOM_ID = int(float(SPLITLINE[0]))
                FULL_OXYGEN_IDS.append(ATOM_ID)
                # get id of atoms initailly positioned in cylinder shell
                for i in range(0, 3):
                    W_POS[i] = float(SPLITLINE[4+i])
                    U_POS[i] = float(SPLITLINE[7+i])
                    FULL_U_POS[ATOM_ID, i] = U_POS[i]
                # Oxygen IDs for water in dumpfiles are multiples of 3 plus 1
                OXY_NUM = int((ATOM_ID-1)/3)
                OXY_W_U_POS[ OXY_NUM, 0] = W_POS[0]
                OXY_W_U_POS[ OXY_NUM, 1] = W_POS[1]
                OXY_W_U_POS[ OXY_NUM, 2] = W_POS[2]
                DIST_AXIS_X = OXY_W_U_POS[ OXY_NUM, 0] - CYL_AXIS_X_INITIAL
                DIST_AXIS_X = DIST_AXIS_X*DIST_AXIS_X
                DIST_AXIS_Y = OXY_W_U_POS[ OXY_NUM, 1] - CYL_AXIS_Y_INITIAL
                DIST_AXIS_Y = DIST_AXIS_Y*DIST_AXIS_Y
                DIST_AXIS = math.sqrt(DIST_AXIS_X+DIST_AXIS_Y)
                DIST_NUMBER = RADIUS_SPLIT*DIST_AXIS/RADIUS_WALL
                DIST_FLOOR = SPHERE_MAX_RADIUS
                DIST_CEIL = BOX_Z_UPP_INITIAL-SPHERE_MAX_RADIUS
                # get id of atoms initailly positioned in cylinder shell
                # write id of atoms initailly positioned in cylinder shell to list
                if (DIST_NUMBER >= RADIUS_FROM) & (DIST_NUMBER < RADIUS_TO):
                    # since considering spheres of radius SPHERE_MAX_RADIUS
                    # it is necessary to consider cylinder boundaries
                    if (U_POS[2] > DIST_FLOOR) & (U_POS[2] < DIST_CEIL):
                        CENTRAL_POS_IDS.append(ATOM_ID)
        #                               0   1    2 3 4 5 6  7  8  9 10 11 12 13 14 15
        elif LINE[:61] == "ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu":
            FROMHERE = True
        elif LINE == "ITEM: BOX BOUNDS pp pp pp\n":
            XFROMHERE = True
            continue
        if XFROMHERE | YFROMHERE | ZFROMHERE:
            if XFROMHERE:
#                print "Getting box parameters from file header nr ", CURR_FILE_NR
                box_x_low = float(LINE.split(" ")[0])
                box_x_upp = float(LINE.split(" ")[1])
#                if CURR_NUM_FILES == 1:
                BOX_X_LOW_INITIAL = box_x_low
                BOX_X_UPP_INITIAL = box_x_upp
                CYL_AXIS_X_INITIAL = (BOX_X_LOW_INITIAL+BOX_X_UPP_INITIAL)*0.5
                XFROMHERE = False
                YFROMHERE = True
                continue
            if YFROMHERE:
                box_y_low = float(LINE.split(" ")[0])
                box_y_upp = float(LINE.split(" ")[1])
#                if CURR_NUM_FILES == 1:
                BOX_Y_LOW_INITIAL = box_y_low
                BOX_Y_UPP_INITIAL = box_y_upp
                CYL_AXIS_Y_INITIAL = (BOX_Y_LOW_INITIAL+BOX_Y_UPP_INITIAL)*0.5
                YFROMHERE = False
                ZFROMHERE = True
                continue
            if ZFROMHERE:
                box_z_low = float(LINE.split(" ")[0])
                box_z_upp = float(LINE.split(" ")[1])
#                if CURR_NUM_FILES == 1:
                BOX_Z_LOW_INITIAL = box_z_low
                BOX_Z_UPP_INITIAL = box_z_upp
                CYL_INITIAL_HEIGHT = BOX_Z_UPP_INITIAL-BOX_Z_LOW_INITIAL
                ZFROMHERE = False
                continue
DUMPF.close()
print "Done reading data."


# --- PART 3: computing pair correlation function from position array and central positions list ---

print "Computing pair correlation function."
# consider the distance from initial positions (near cylinder axis) to
# the cylinders lateral surface. If RADIUS_WALL would be the max radius
# for the sphere, then we would look at a volume outside of the cylinder,
# leading to a systematical error in computing the pair correlation.
NUMBER_SPHERES = len(CENTRAL_POS_IDS)
# SPHERE_SHELL_COUNTER[i,j] = number of atoms in shell j around atom i
SPHERE_SHELL_COUNTER = numpy.zeros(( NUMBER_SPHERES, SPHERE_SPLIT))
# SPHERE_COUNTER_SUM[i] = total number of atoms in sphere around atom i
SPHERE_COUNTER_SUM = numpy.zeros(NUMBER_SPHERES)
# SPHERE_SHELL_COUNTER_SUM[i] = total number of atoms in sphere shell i
SPHERE_SHELL_COUNTER_SUM = numpy.zeros(SPHERE_SPLIT)
ATOM_COUNTER = 0
for CENT_ATOM_ID in CENTRAL_POS_IDS:
    ATOM_COUNTER += 1
    print "Currently at atom ", ATOM_COUNTER, " out of ", NUMBER_SPHERES
    for ATOM_ID in FULL_OXYGEN_IDS:
        if ATOM_ID == CENT_ATOM_ID:
            continue
        FULL_DIST = 0.0
        for i in range(0, 3):
            DIST = FULL_U_POS[CENT_ATOM_ID, i] - FULL_U_POS[ATOM_ID, i]
            FULL_DIST += DIST*DIST
        FULL_DIST = math.sqrt(FULL_DIST)
        if FULL_DIST <= SPHERE_MAX_RADIUS:
            DIST_NUMBER = int(math.floor(FULL_DIST*SPHERE_SPLIT/SPHERE_MAX_RADIUS))
            SPHERE_SHELL_COUNTER[ ATOM_COUNTER-1, DIST_NUMBER] += 1
            SPHERE_COUNTER_SUM[ATOM_COUNTER-1] += 1
            SPHERE_SHELL_COUNTER_SUM[DIST_NUMBER] += 1
print "Done counting atoms in sphere shells."

# sum up atoms (in all spheres) in shell j divided by total number of atoms in sphere i
# to get number density of atoms in shell j, then divide it by number of spheres
# MEAN_MOLECULES_PER_SPHERE_SHELL[j] = mean number density in shell j
MEAN_MOLECULES_PER_SPHERE_SHELL = numpy.zeros(SPHERE_SPLIT)
#for j in range(0, SPHERE_SPLIT):
#    for i in range(0, NUMBER_SPHERES):
#         MEAN_MOLECULES_PER_SPHERE_SHELL[j] += SPHERE_SHELL_COUNTER[ i, j]/SPHERE_COUNTER_SUM[i]
#MEAN_MOLECULES_PER_SPHERE_SHELL /= float(NUMBER_SPHERES)
MEAN_MOLECULES_PER_SPHERE_SHELL = SPHERE_SHELL_COUNTER_SUM/float(NUMBER_SPHERES)
print "Done computing pair correlation function."

# --- PART 4: Write pair correlation data to file ---

print "Writing data to file."
FIRSTLINE = "r [Å]"
FIRSTLINE += " ; g(r)"
FIRSTLINE += " ; molecules in shell n [# molecules]"
for i in range(0, len(CENTRAL_POS_IDS)):
    FIRSTLINE += " ; mol. around oxygen "+str(i)+" [# molecules]"
FIRSTLINE += "\n"
PAIR_CORR_OUTFILE_NAME = "data/pair_correlation_from_dumpfile"
PAIR_CORR_OUTFILE_NAME += "-from_"+str(RADIUS_FROM)
PAIR_CORR_OUTFILE_NAME += "_to_"+str(RADIUS_TO)
PAIR_CORR_OUTFILE_NAME += "-"+str(DUMPFILE.split("/")[-1])
PAIR_CORR_OUTFILE_NAME = PAIR_CORR_OUTFILE_NAME.split(".data")[0]
PAIR_CORR_OUTFILE_NAME += ".csv"

PAIR_CORR_OUTFILE = open(PAIR_CORR_OUTFILE_NAME, "w")
PAIR_CORR_OUTFILE.write(FIRSTLINE)
D_R = SPHERE_MAX_RADIUS/SPHERE_SPLIT
# The mean volume of one water molecule is 1/29.915 1/Å³
PRE_FACTOR = MACRO_DENSITY/29.915*4.0*math.pi*D_R
for R_i in range(0, SPHERE_SPLIT):
    SPHERE_SHELL = (R_i+1)*D_R
    DIVISOR = PRE_FACTOR*SPHERE_SHELL*SPHERE_SHELL
    MOLECULES_PER_SHELL_VOLUME = MEAN_MOLECULES_PER_SPHERE_SHELL[R_i]/DIVISOR
    HELPSTRING = str(SPHERE_SHELL)
    HELPSTRING += " ; "+str(MOLECULES_PER_SHELL_VOLUME)+" "
    HELPSTRING += " ; "+str(SPHERE_SHELL_COUNTER_SUM[R_i])+" "
    for i in range(0, NUMBER_SPHERES):
        HELPSTRING += " ; "+str(SPHERE_SHELL_COUNTER[i, R_i])
    HELPSTRING += "\n"
#    print HELPSTRING
    PAIR_CORR_OUTFILE.write(HELPSTRING)
PAIR_CORR_OUTFILE.close()
print "Done writing data to file: ", PAIR_CORR_OUTFILE_NAME
