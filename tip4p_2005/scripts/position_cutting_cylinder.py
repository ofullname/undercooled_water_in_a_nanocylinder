#!/usr/bin/python
# coding=utf-8

import sys
import math
import io
import shutil
import os


# needed input file: lammps data file created by "write_data" command
# in the file head three lines with the box sizes
# after the file head the atom positions are listed as lines
# lines look like: ID angle_id charge atom_type x_pos y_pos z_pos ...
#print(sys.argv[1])
positions_infile=sys.argv[1]
radius_reduce=float(sys.argv[2])

# when cutting out only look at Oxygen atoms!
# do not cut out the Hydrogen atoms by themselve.
# when cutting out Oxygen do not forget their Hydrogen partners!
# Oxygen number: if modulo_3(atom_number)==1; i.e.:
# atom number 1 = Oxygen
# atom number 2 = Hydrogen
# atom number 3 = Hydrogen
# atom number 4 = Oxygen
# atom number 5 = Hydrogen
# etc.

# get cylinder maximal radius and axis position from box sizes
read_box_x_size=False
read_box_y_size=False
the_radius=0.0
the_max_radius=0.0
with open(positions_infile,'r') as in_f:
   for line in in_f:
      if len(line.rsplit()) == 2:
         if line.split(" ")[1] == "atoms\n":
            number_atoms = int(float(line.split(" ")[0]))
            number_molecules = int(float(line.split(" ")[0])/3.0)
      elif len(line.rsplit()) == 4:
         if line.split(" ")[2] == "xlo":
            x_low = float(line.split(" ")[0])
            x_upp = float(line.split(" ")[1])
            read_box_size_x=True
         if line.split(" ")[2] == "ylo":
            y_low = float(line.split(" ")[0])
            y_upp = float(line.split(" ")[1])
            the_max_radius = float(min(x_upp-x_low,y_upp-y_low)*0.5)
            read_box_y_size=True
            break
      elif (read_box_x_size & read_box_y_size) == True:
         break

the_radius = the_max_radius-radius_reduce
max_radius = the_radius**2
mid_x = x_low+0.5*x_upp
mid_y = y_low+0.5*y_upp
#print(the_radius)
if (float(the_radius) <= 0.0):
   sys.exit("ERROR: radius calculation from simulation box size is incorrect")
print("Found maximal radius for cylinder: "+str(the_max_radius))
print("Reducing maximal radius for cylinder by "+str(radius_reduce)+" units.")
print("Resulting in used radius for position cutting: "+str(the_radius))

# write list of oxygen atoms ID outside of cylinder
# oxygen_outside_bonds = list of oxygen atoms ID positioned outside of cylinder
# len(oxygen_outside_bonds) = number of molecules positioned outside of cylinder
print("Creating list of Oxygen atoms outside of cylinder.")
fromhere = False
oxygen_outside_bonds_array=[0] * number_atoms
atom_from_molecule_outside_bonds_array = [0] * number_atoms
oxygen_outside_bonds = []
atom_from_molecule_outside_bonds = []
with open(positions_infile,'r') as in_f:
   for line in in_f:
      if fromhere:
         if (len(line.split(" ")) == 10):
            atom_type=float(line.split(" ")[2])
            xpos=float(line.split(" ")[4])
            ypos=float(line.split(" ")[5])
            if (((xpos-mid_x)**2+(ypos-mid_y)**2) >= max_radius) & (atom_type ==2):
               atom_number=int(float(line.split(" ")[0]))
               oxygen_outside_bonds_array[int(atom_number)-1]=1
               oxygen_outside_bonds.append(int(atom_number))
               atom_from_molecule_outside_bonds_array[int(atom_number)-1]=1
               atom_from_molecule_outside_bonds_array[int(atom_number)]=1
               atom_from_molecule_outside_bonds_array[int(atom_number)+1]=1
#               atom_from_molecule_outside_bonds.append(atom_number)
#               atom_from_molecule_outside_bonds.append(atom_number+1)
#               atom_from_molecule_outside_bonds.append(atom_number+2)
            continue
         elif str(line) == "Velocities\n":
            print("Done reading atom positions.")
            break
         else:
            continue
      elif str(line) == "Atoms # full\n":
         print("Beginning to read atom positions.")
         fromhere = True

if len(oxygen_outside_bonds) < 1:
   sys.exit("Aborting: there are no atoms to be cut out.")

#sorted_atom_outside_bonds=sorted(list(set(atom_from_molecule_outside_bonds)))
#sorted_oxygen_outside_bonds=sorted(list(set(oxygen_outside_bonds)))
number_removed_oxygens=int(len(oxygen_outside_bonds))
number_remaining_atoms=number_atoms-3*number_removed_oxygens
number_remaining_molecules=number_molecules-number_removed_oxygens
print("Initial number of molecules: "+str(number_molecules))
print("Number of molecules to be removed: "+str(number_removed_oxygens))
print("Number of remaining molecules: "+str(number_remaining_molecules))


if (number_remaining_atoms <= 0):
   sys.exit("ERROR: NUMBER OF REMAINING ATOMS MUST BE > 0")
if (number_remaining_atoms % 3) != 0:
   sys.exit("ERROR: NUMBER OF REMAINING ATOMS HAS TO BE MULTIPLE OF 3!")


# Create array to help with rearranging IDs of remaining atoms.
# Decrease the remaining atoms ID by: 3 times the number of oxygen atoms removed with lower ID.
print("Calculating ID shifts for remaining atoms.")
id_shift_array=[0] * number_atoms
outside_molecule_number=0
#help_sorted_oxygen_outside_bonds=sorted(list(set(oxygen_outside_bonds)))
for i in range(0, number_molecules):
   atom_id=i*3+1
#   if int(atom_id) in help_sorted_oxygen_outside_bonds:
#      help_sorted_oxygen_outside_bonds.remove(atom_id)
   if oxygen_outside_bonds_array[atom_id-1]:
      id_shift_array[atom_id]=number_atoms+1
      id_shift_array[atom_id+1]=number_atoms+1
      id_shift_array[atom_id-1]=number_atoms+1
      outside_molecule_number=outside_molecule_number+1
   else:
      id_shift_array[atom_id]=outside_molecule_number*3
      id_shift_array[atom_id+1]=outside_molecule_number*3
      id_shift_array[atom_id-1]=outside_molecule_number*3
if not (outside_molecule_number == len(oxygen_outside_bonds)):
   print(outside_molecule_number, len(oxygen_outside_bonds))
#if not (outside_molecule_number == len(oxygen_outside_bonds_array)):
#   print(outside_molecule_number, len(oxygen_outside_bonds_array))
   sys.exit("ERROR WHILE CHECKING NUMBER OF ATOMS OUTSIDE BONDS")


print("Writing remaining atoms with rearranged IDs to position file.")
renumberedpositionsfile="trajectories/H2O_"+str(number_remaining_molecules)+"-radius_"+str(the_radius)+"-cylinder_positions.data"
#renumberedpositionsfile="trajectories/H2O_"+str(number_remaining_molecules)+"-NpT-p_"+positions_infile.split("-p_")[1].split(".data")[0]+"-radius_"+str(the_radius)+"-cylinder_positions.data"
#if os.path.isfile(renumberedpositionsfile) == True:
#   print("File with name already exists.")
#   print("Should the following file be overwritten?\n"+str(renumberedpositionsfile))
#   while True:
#      read_input=input("y/n\n")
#      if (read_input == "y") | (read_input == "Y"):
#         print("Replacing old file with new one.")
#         break
#      elif (read_input == "n") | (read_input == "N"):
#         sys.exit("File will not be overwritten. Aborting.")
#      else:
#         print("Input unclear. Please try again.")


header=True
atomsfromhere = False
velostart = False
bondsstart = False
anglesstart = False
isinlist = False
bondnumber = 0
anglenumber = 0
new_pos_new_id = open(renumberedpositionsfile, "w")
with open(positions_infile,'r') as in_f:
   for line in in_f:
      if header:
         if (int(len(line.split(" "))) > 1):
            if line.split(" ")[1] == "atoms\n":
               new_pos_new_id.write(str(int(number_remaining_atoms))+" atoms\n")
            elif line.split(" ")[1] == "bonds\n":
               new_pos_new_id.write(str(int(2*number_remaining_molecules))+" bonds\n")
            elif line.split(" ")[1] == "angles\n":
               new_pos_new_id.write(str(int(number_remaining_molecules))+" angles\n")
            elif line == "Atoms # full\n":
               new_pos_new_id.write(line)
               print("Atoms start")
               header = False
               atomsfromhere = True
               continue
            else:
               new_pos_new_id.write(line)
         else:
            new_pos_new_id.write(line)
      elif atomsfromhere:
         if (len(line.split(" ")) == 10):
            atom_id=int(line.split(" ")[0])
            if (int(id_shift_array[atom_id-1]) > int(number_atoms)):
               continue # if atom is outside constraints do not write line
            new_number=atom_id-int(id_shift_array[int(atom_id-1)])
            helpstring=""
            for word in line.rsplit()[1:]:
               helpstring = helpstring +" "+str(word)
            new_pos_new_id.write(str(int(new_number))+helpstring+"\n")
         elif (line == "Velocities\n"):
            print("Done with positions.")
            new_pos_new_id.write(line)
            atomsfromhere=False
            velostart=True
            continue
         else:
            new_pos_new_id.write(line)
            continue
      elif velostart:
         if (len(line.split(" ")) >= 3):
            atom_id=int(line.split(" ")[0])
            if (int(id_shift_array[int(atom_id-1)]) > int(number_atoms)): 
               continue # if atom is outside constraints do not write line
            new_number=atom_id-int(id_shift_array[int(atom_id-1)])
            helpstring=""
            for word in line.rsplit()[1:]:
               helpstring = helpstring +" "+str(word)
            new_pos_new_id.write(str(int(new_number))+helpstring+"\n")
         elif line == "Bonds\n":
            new_pos_new_id.write(line)
            print("Done with velocities.")
            print("Bonds start")
            velostart=False
            bondsstart=True
            continue
         else:
            new_pos_new_id.write(line)
            continue
      elif bondsstart:
         if (len(line.split(" ")) == 4):
            atom_id_1=int(line.split(" ")[2])
            if (int(id_shift_array[atom_id_1-1]) > int(number_atoms)): 
               continue # if atom is outside constraints do not write line
            new_number_1=atom_id_1-int(id_shift_array[int(atom_id_1-1)])
            atom_id_2=int(line.split(" ")[3])
            new_number_2=atom_id_2-int(id_shift_array[int(atom_id_2-1)])
#            print(atom_id_1, new_number_1, atom_id_2, new_number_2)
            bondnumber=bondnumber+1
            helpstring=str(bondnumber)+" 1 "
            helpstring=helpstring+str(int(new_number_1))+" "+str(int(new_number_2))
            new_pos_new_id.write(helpstring+"\n")
         elif line == "Angles\n":
            new_pos_new_id.write(line)
            print("Angles start")
            bondsstart=False
            anglesstart=True
            continue
         else:
            new_pos_new_id.write(line)
            continue
      elif anglesstart:
         if (len(line.rsplit()) == 5):
            atom_id=int(line.split(" ")[3])
            if (int(id_shift_array[atom_id-1]) > int(number_atoms)): 
               continue # if atom is outside constraints do not write line
            new_number=atom_id-int(id_shift_array[int(atom_id-1)])
            anglenumber=anglenumber+1
            helpstring=str(anglenumber)+" 1 "
            helpstring=helpstring+str(int(new_number+1))+" "+str(int(new_number))+" "+str(int(new_number+2))
            new_pos_new_id.write(helpstring+"\n")
         else:
            new_pos_new_id.write(line)
            continue
      elif not (header or atomsfromhere) or (anglesstart or velostart):
         sys.exit("ERROR WHILE WRITING NEW POSITIONS AND IDs TO FILE")

print(anglenumber, bondnumber)

new_pos_new_id.close()
in_f.close()
print("Copying cylinder position file to folder \"starting_positions\"")
shutil.copyfile(renumberedpositionsfile, "starting_positions/"+str(renumberedpositionsfile).split("/")[-1])
print("Done.")

