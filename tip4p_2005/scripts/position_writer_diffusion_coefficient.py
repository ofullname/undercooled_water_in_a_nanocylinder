#!/bin/pyhton3
# coding=utf-8

import sys
import math
import io
import numpy
import matplotlib.pyplot as plt


# calculate mean square displacement for Oxygen atoms in cylinders-shells positioned in outer shell at beginning
# posfile_list = file containing list of lammps dump files
#
# the box dimensions are taken from the file header
# ITEM: BOX BOUNDS pp pp pp
# information in the lines following the header have to look like this:
# ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu <optional_other_variables>
#
# radius_split: in how many segments the radius should be split
# i.e. the thickness of the cylinder shells = radius / radius_split


posfile_list = sys.argv[1]
radius_split = float(sys.argv[2])
print("Found file "+str(posfile_list))
simulation_timestep=1.0                 # timestep from one position file to the other in ps

# get box size and cylinder axis from lammps data file
# get oxygen atoms position (ID=2) from lammps data file
# calculate distance from cylinder axis to oxygen atom
print("Reading data from file.")
num_files=0.0
radius_max=0.0
innerpart=math.ceil(0.0*radius_split)   # only consider Molecules in intermediate cylinder shell
outerpart=math.ceil(0.5*radius_split)
atom_inside_number=0.0
inside_id_list=[]
inside_id_dict={}
inside_pos_list=[]
with open(posfile_list, 'r') as posf_list:
   for line_posf_list in posf_list:
      line_posf_list=line_posf_list[:-1]  # [:-1] to ignore the "\n" written to the file at the end of each line
      print("Reading data from file: "+str(line_posf_list))
      with open(line_posf_list, 'r') as posf:
         num_files += 1.0
         posfile = str(posf)
         xfromhere=False
         yfromhere=False
         zfromhere=False
         fromhere=False
#         copy_inside_id_list=inside_id_list.copy()
         copy_inside_id_list=inside_id_list[:]
         for line in posf:
            if ( fromhere ):
               if ( line.split(" ")[2] == "2" ): # Only Atoms of this type are considered (1= Hydrogen, 2=Oxygen)
                  atom_id = int(float(line.split(" ")[0]))
                  if (num_files == 1.0) :
                     xu_pos = float(line.split(" ")[7])
                     yu_pos = float(line.split(" ")[8])
                     xsu_pos = float(line.split(" ")[13])
                     ysu_pos = float(line.split(" ")[14])
                     rad_pos = (xu_pos-achse_x)**2 + (yu_pos-achse_y)**2
                     rad_pos = math.sqrt(rad_pos)
                     partial_area_number = int(math.floor(rad_pos*radius_split/radius_max))
                     if ( (partial_area_number >= innerpart) & (partial_area_number <= outerpart) ):
                        atom_inside_number+=1.0
                        zu_pos = float(line.split(" ")[9])
                        zsu_pos = float(line.split(" ")[15])
                        inside_id_list.append(atom_id)
                        inside_id_dict.update({atom_id:int(atom_inside_number)})
                        helperino=str(num_files)+" ; "+str(atom_id)+" ; "+str(atom_inside_number)+" ; "+str(xu_pos)+" ; "+str(yu_pos)+" ; "+str(zu_pos)+" ; "+str(xsu_pos)+" ; "+str(ysu_pos)+" ; "+str(zsu_pos)
                        inside_pos_list.append(helperino)
                        #print(helperino)
                        continue
                     continue
                  else:
                     if (atom_id in copy_inside_id_list):
                        xu_pos = float(line.split(" ")[7])
                        yu_pos = float(line.split(" ")[8])
                        zu_pos = float(line.split(" ")[9])
                        xsu_pos = float(line.split(" ")[13])
                        ysu_pos = float(line.split(" ")[14])
                        zsu_pos = float(line.split(" ")[15])
                        atom_inside_number=inside_id_dict[atom_id]
                        helperino=str(num_files)+" ; "+str(atom_id)+" ; "+str(atom_inside_number)+" ; "+str(xu_pos)+" ; "+str(yu_pos)+" ; "+str(zu_pos)+" ; "+str(xsu_pos)+" ; "+str(ysu_pos)+" ; "+str(zsu_pos)
                        inside_pos_list.append(helperino)
                        #print(copy_inside_id_list, atom_id)
                        copy_inside_id_list.remove(atom_id)
                        #print(copy_inside_id_list)
                        #print(helperino)
                        continue
                  continue
               continue
            elif (line[0:40] == "ITEM: ATOMS id mol type q x y z xu yu zu"):
               fromhere=True
               continue
            elif ( line == "ITEM: BOX BOUNDS pp pp pp\n" ):
               xfromhere=True
               continue
            elif ( xfromhere | yfromhere | zfromhere):
               if ( xfromhere ):
                  box_x_low = float(line.split(" ")[0])
                  box_x_upp = float(line.split(" ")[1])
                  xfromhere=False
                  yfromhere=True
                  continue
               elif ( yfromhere ):
                  box_y_low = float(line.split(" ")[0])
                  box_y_upp = float(line.split(" ")[1])
                  yfromhere=False
                  zfromhere=True
                  continue
               elif ( zfromhere ):
                  box_z_low = float(line.split(" ")[0])
                  box_z_upp = float(line.split(" ")[1])
                  radius_max_box = min((box_x_upp-box_x_low), (box_y_upp-box_y_low))*0.5 
                  radius_max = max(radius_max, radius_max_box)
                  cyl_height = (box_z_upp-box_z_low)
                  if (num_files == 1.0):
                     achse_x = (box_x_upp+box_x_low)*0.5
                     achse_y = (box_y_upp+box_y_low)*0.5
                     cyl_initial_height = (box_z_upp-box_z_low)
                  zfromhere = False
                  continue
      posf.close()
posf_list.close()


print("Done reading data.")
radius=radius_max
print("Found maximum radius "+str(radius)+" and splitting it "+str(radius_split)+" times.")
print("Number of files read: "+str(num_files))
num_inside=len(inside_id_list)
print("Number of molecules used for calculation: "+str(num_inside))


print("Calculating distance from cylinder axis.")
xu_time_matrix=numpy.zeros((int(num_files), int(num_inside)))
yu_time_matrix=numpy.zeros((int(num_files), int(num_inside)))
zu_time_matrix=numpy.zeros((int(num_files), int(num_inside)))
xsu_time_matrix=numpy.zeros((int(num_files), int(num_inside)))
ysu_time_matrix=numpy.zeros((int(num_files), int(num_inside)))
zsu_time_matrix=numpy.zeros((int(num_files), int(num_inside)))
for foo in inside_pos_list:
# inside_pos_list.append(str(num_files)+" ; "+str(atom_id)+" ; "+str(atom_inside_number)+" ; "+str(x_pos)+" ; "+str(y_pos)+" ; "+str(z_pos)+" ; "+str(xu_pos)+" ; "+str(yu_pos)+" ; "+str(zu_pos))
   t_step=int(float(foo.split(" ; ")[0])-1.0)
   atom_id=int(float(foo.split(" ; ")[1]))
   atom_number=int(float(foo.split(" ; ")[2])-1.0)
   xu_pos=float(foo.split(" ; ")[3])
   yu_pos=float(foo.split(" ; ")[4])
   zu_pos=float(foo.split(" ; ")[5])
   xsu_pos=float(foo.split(" ; ")[6])
   ysu_pos=float(foo.split(" ; ")[7])
   zsu_pos=float(foo.split(" ; ")[8])
   xu_time_matrix[(t_step,atom_number)]=xu_pos
   yu_time_matrix[(t_step,atom_number)]=yu_pos
   zu_time_matrix[(t_step,atom_number)]=zu_pos
   xsu_time_matrix[(t_step,atom_number)]=xsu_pos
   ysu_time_matrix[(t_step,atom_number)]=ysu_pos
   zsu_time_matrix[(t_step,atom_number)]=zsu_pos


innerrad=radius*innerpart/radius_split
outerrad=radius*outerpart/radius_split
posfilename=str("./data/"+posfile_list.split("/")[-1].split("[0-9]*.data")[0]+"-position_time_series-initial_molecules_distance_from_cylinder_axis_from_"+str(innerrad)+"_to_"+str(outerrad)+".data")
print("Writing positions data to file: "+str(posfilename))
with open(posfilename, 'a+') as posf:
   firstline="t_step [ps]"
   for i in range(0, int(num_inside)):
      firstline=str(firstline)+" ; xu_atom_"+str(i)+" [Å] ; yu_atom_"+str(i)+" [Å] ; zu_atom_"+str(i)+"  [Å]"
   posf.write(str(firstline)+"\n")
   for t_step in range(0, int(num_files)):
      posstring=str(simulation_timestep*t_step)
      for atom_number in range(0, int(num_inside)):
         helppos=" ; "+str(xu_time_matrix[(t_step,atom_number)])+" ; "+str(yu_time_matrix[(t_step,atom_number)])+" ; "+str(zu_time_matrix[(t_step,atom_number)])
         posstring=posstring+str(helppos)
      posstring += "\n"
      posf.write(posstring)
posf.close()


radposfilename=str("./data/"+posfile_list.split("/")[-1].split("[0-9]*.data")[0]+"-initial_molecules_distance_from_cylinder_axis_from_"+str(innerrad)+"_to_"+str(outerrad)+".data")
print("Writing MSD data to file: "+str(radposfilename))
with open(radposfilename, 'a+') as radposf:
   firstline="t_step [ps]"
   for i in range(0, int(num_inside)):
      firstline=str(firstline)+" ; R_axis_atom_"+str(i)+" [Å]"
   radposf.write(str(firstline)+"\n")
   for t_step in range(0, int(num_files)):
      radiusstring=str(simulation_timestep*t_step)
      for atom_number in range(0, int(num_inside)):
         helprad=math.sqrt((xu_time_matrix[(t_step,atom_number)]-achse_x)**2.0+(yu_time_matrix[(t_step,atom_number)]-achse_y)**2.0)
         radiusstring += " ; "+str(helprad)
      radiusstring += "\n"
      radposf.write(radiusstring)
radposf.close()

