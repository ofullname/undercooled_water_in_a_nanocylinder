#!/bin/pyhton3
# coding=utf-8

import sys
import math
import io
import numpy


# calculate mean square displacement for Oxygen atoms in cylinders-shells positioned in outer shell at beginning
# posfile_list = file containing list of lammps dump files
#
# the box dimensions are taken from the file header
# ITEM: BOX BOUNDS pp pp pp
# information in the lines following the header have to look like this:
# ITEM: ATOMS id mol type q x y z xu yu zu xs ys zs xsu ysu zsu ... <optional_other_variables> 
#
# radius_split: in how many segments the radius should be split
# i.e. the thickness of the cylinder shells = radius / radius_split


posfile_list = sys.argv[1]
print("Found file "+str(posfile_list))


# get box size and cylinder axis from lammps data file
# get oxygen atoms position (ID=2) from lammps data file
# calculate distance from cylinder axis to oxygen atom
print("Reading data from file.")
num_files=0.0
box_size_list=[]
box_size_time_dict={}
with open(posfile_list, 'r') as posf_list:
   for line_posf_list in posf_list:
      line_posf_list=line_posf_list[:-1]  # [:-1] to ignore the "\n" written to the file at the end of each line
#      print("Reading data from file: "+str(line_posf_list))
      with open(line_posf_list, 'r') as posf:
         num_files += 1.0
         xfromhere=False
         yfromhere=False
         zfromhere=False
         for line in posf:
            if (line[0:40] == "ITEM: ATOMS id mol type q x y z xu yu zu"):
               break
            elif ( line == "ITEM: BOX BOUNDS pp pp pp\n" ):
               xfromhere=True
               continue
            elif ( xfromhere | yfromhere | zfromhere):
               if ( xfromhere ):
                  box_x_low = float(line.split(" ")[0])
                  box_x_upp = float(line.split(" ")[1])
                  xfromhere=False
                  yfromhere=True
                  continue
               elif ( yfromhere ):
                  box_y_low = float(line.split(" ")[0])
                  box_y_upp = float(line.split(" ")[1])
                  yfromhere=False
                  zfromhere=True
                  continue
               elif ( zfromhere ):
                  box_z_low = float(line.split(" ")[0])
                  box_z_upp = float(line.split(" ")[1])
                  boxsizes=str(str(num_files)+" ; "+str(box_x_low)+" ; "+str(box_x_upp)+" ; "+str(box_y_low)+" ; "+str(box_y_upp)+" ; "+str(box_z_low)+" ; "+str(box_z_upp))
                  box_size_time_dict[int(num_files)]=str(line_posf_list)
                  box_size_list.append(boxsizes)
                  break
      posf.close()
posf_list.close()
print("Done reading data.")
print("Number of files read: "+str(num_files))


mean_vol=0.0
mean_vol_square=0.0
box_size_time_matrix=numpy.zeros((int(num_files), 6))
for foo in box_size_list:
   t_step=int(float(foo.split(" ; ")[0]))-1
   for i in range(6):
      box_size_time_matrix[(int(t_step),int(i))]=float(foo.split(" ; ")[int(i)+1])
   mean_vol+=float(foo.split(" ; ")[6])-float(foo.split(" ; ")[5])
   mean_vol_square+=(float(foo.split(" ; ")[6])-float(foo.split(" ; ")[5]))**2.0
mean_vol=mean_vol/num_files
mean_vol_square=mean_vol_square/num_files
mean_vol_var=math.sqrt(mean_vol_square-(mean_vol)**2.0)
print("Found mean volume "+str(mean_vol)+" +/- "+str(mean_vol_var))

vol_diff=numpy.zeros(int(num_files))
for i in range(int(num_files)):
   diff=box_size_time_matrix[(int(i),5)]-box_size_time_matrix[(int(i),4)]
   diff=(mean_vol-diff)
   vol_diff[int(i)]=diff

#print(vol_diff)

min_diff=vol_diff[0]
min_diff_index=0
min_positive_diff=abs(vol_diff[0])
min_positive_diff_index=0
for i in range(1,int(num_files)):
   if (abs(vol_diff[int(i)]) < abs(min_diff)):
      min_diff=vol_diff[int(i)]
      min_diff_index=i
   if ( vol_diff[int(i)] >= 0.0 ):
      if ( vol_diff[int(i)] < min_positive_diff ):
         min_positive_diff=vol_diff[int(i)]
         min_positive_diff_index=i
         
print("Found minimal volume difference to mean volume "+str(min_diff)+" at time step "+str(min_diff_index))
print("I.e. for file: "+str(box_size_time_dict[int(min_diff_index)]))

print("Found minimal positive volume difference to mean volume "+str(min_positive_diff)+" at time step "+str(min_positive_diff_index))
print("I.e. for file: "+str(box_size_time_dict[int(min_positive_diff_index)]))


