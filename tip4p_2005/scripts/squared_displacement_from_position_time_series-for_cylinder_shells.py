#!/bin/pyhton3
# coding=utf-8

"""
Read atom positions from time series file and calculate square displacement.
One line per timestep.
Each column corresponding to an atoms coordinate:
t[ps] sd_x_atom_1[Å²] sd_y_atom_1[Å²] sd_z_atom_1[Å²] sd_x_atom_2[Å²] ...

The name of the created file contains information about the simulation:
msd-<infile>-<number_molecules>-<number_timesteps>-<box_parametes>.csv

POSFILE_LIST = file containing list of lammps dump files
"""

import sys
import numpy

POSFILE = sys.argv[1]
NUMBER_MOLECULES = int(float(sys.argv[2]))  # number of molecules used in simulation
print "Found position time series file "+str(POSFILE)
print "Found number of molecules "+str(NUMBER_MOLECULES)

SD_OUTFILE_NAME = ".data/square_displacement-"
SD_OUTFILE_NAME += str(POSFILE)+".csv"

LINE_NUM = 0
ATOM_ID_DICT = {}
POS_DICT = {}
XU_TIME_MATRIX = numpy.zeros((2, NUMBER_MOLECULES))
YU_TIME_MATRIX = numpy.zeros((2, NUMBER_MOLECULES))
ZU_TIME_MATRIX = numpy.zeros((2, NUMBER_MOLECULES))

print "Reading data from file and writing it to position time series file."
with open(POSFILE, 'r') as POSF:
    for LINE_POSF in POSF:
        LINE_NUM += 1
        if LINE_NUM == 1:
            FIRSTLINE = "t[ps]"
            for i in range(1, NUMBER_MOLECULES+1):
                FIRSTLINE += " ; sd_x_atom_"+str(i)+"[Å²]"
                FIRSTLINE += " ; sd_y_atom_"+str(i)+"[Å²]"
                FIRSTLINE += " ; sd_z_atom_"+str(i)+"[Å²]"
            OUTFILE = open(SD_OUTFILE_NAME, "w")
            OUTFILE.write(FIRSTLINE)
            OUTFILE.close()
            continue
        elif LINE_NUM == 2:
            LINE_OUTFILE = "\n"+LINE_POSF.split(" ; ")[0]
            for i in range(0, NUMBER_MOLECULES):
                XU_TIME_MATRIX[(0, i)] = LINE_POSF.split(" ; ")[3*i+1]
                YU_TIME_MATRIX[(0, i)] = LINE_POSF.split(" ; ")[3*i+2]
                ZU_TIME_MATRIX[(0, i)] = LINE_POSF.split(" ; ")[3*i+3]
                LINE_OUTFILE += " ; "+0.0
                LINE_OUTFILE += " ; "+0.0
                LINE_OUTFILE += " ; "+0.0
            OUTFILE = open(SD_OUTFILE_NAME, "a")
            OUTFILE.write(LINE_OUTFILE)
            OUTFILE.close()
            continue
        elif LINE_NUM > 2:
            LINE_OUTFILE = "\n"+LINE_POSF.split(" ; ")[0]
            for i in range(0, NUMBER_MOLECULES):
                XU_TIME_MATRIX[(1, i)] = LINE_POSF.split(" ; ")[3*i+1]
                YU_TIME_MATRIX[(1, i)] = LINE_POSF.split(" ; ")[3*i+2]
                ZU_TIME_MATRIX[(1, i)] = LINE_POSF.split(" ; ")[3*i+3]
                XU_DIFF = XU_TIME_MATRIX[(1, i)]-XU_TIME_MATRIX[(0, i)]
                YU_DIFF = YU_TIME_MATRIX[(1, i)]-YU_TIME_MATRIX[(0, i)]
                ZU_DIFF = ZU_TIME_MATRIX[(1, i)]-ZU_TIME_MATRIX[(0, i)]
                XU_DIFF = XU_DIFF*XU_DIFF
                YU_DIFF = YU_DIFF*YU_DIFF
                ZU_DIFF = ZU_DIFF*ZU_DIFF
                LINE_OUTFILE += " ; "+str(XU_DIFF)
                LINE_OUTFILE += " ; "+str(YU_DIFF)
                LINE_OUTFILE += " ; "+str(ZU_DIFF)
            OUTFILE = open(SD_OUTFILE_NAME, "a")
            OUTFILE.write(LINE_OUTFILE)
            OUTFILE.close()
            continue
POSF.close()

print "Done reading data."
