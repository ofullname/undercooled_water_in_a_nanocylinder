#!/bin/bash
#
#SBATCH -J msd-avg-cyl
#SBATCH -N 1
#SBATCH --ntasks-per-node=24
#SBATCH --ntasks-per-core=1
#SBATCH --partition=mem_0096
#SBATCH --qos=mem_0096
#SBATCH --time=0:25:00

# when srun is used, you need to set:
#srun -l -N2 -n32 a.out
# or
#mpirun -np 32 a.out
np=24


# infile = lammps dump file
# the box dimensions are taken from the file header
# ITEM: BOX BOUNDS pp pp pp
# information in the lines following the header have to look like this:
# ITEM: ATOMS id type x y z vx vy vz ix iy iz
module purge
module load gcc/4.8.5 intel-mkl/2019 python/2.7.11 numpy/1.16.5
#module load python/3.8.0-gcc-9.1.0-wkjbtaa py-numpy/1.18.4-intel-19.0.5.281-cxjynhy
#module load gcc/7.3 intel-mkl/2018 python/3.7 numpy/1.15.4

filelist=${1}
number_molecules=${2}
time_step=${3}
radius_wall=${4}
num_split=${5}
split_from=${6}
split_to=${7}

python scripts/msd_averaged_from_dump_files_cylinder_shell.py "${filelist}" "${number_molecules}" "${time_step}" "${radius_wall}" "${num_split}" "${split_from}" "${split_to}"

