#!/bin/bash
#
#SBATCH -J pos_cut
#SBATCH -N 2
#SBATCH --ntasks-per-node=24
#SBATCH --ntasks-per-core=1
#SBATCH --partition=mem_0096
#SBATCH --qos=devel_0096

# when srun is used, you need to set:
#srun -l -N2 -n32 a.out
# or
#mpirun -np 32 a.out
np=48

inputfile_for_cutting=${1}
position_cutting_cutoff_radius=${2}
#varsfile=${2}
#. varsfile

#source modules
module purge
module load gcc/4.8.5 intel-mkl/2019 python/2.7.11 numpy/1.16.5

python scripts/position_cutting_cylinder.py "${inputfile_for_cutting}" ${position_cutting_cutoff_radius}
