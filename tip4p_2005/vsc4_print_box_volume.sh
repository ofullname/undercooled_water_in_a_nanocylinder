#!/bin/bash
#
#SBATCH -J chk
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --ntasks-per-core=2
#SBATCH --mail-type=BEGIN    # first have to state the type of event to occur 
#SBATCH --mail-user=<email@address.at>   # and then your email address
#SBATCH --partition=mem_0128
#SBATCH --qos=devel_0128

# when srun is used, you need to set:
#srun -l -N2 -n32 a.out
# or
#mpirun -np 32 a.out
np=8


# infile = lammps dump file
# the box dimensions are taken from the file header
# ITEM: BOX BOUNDS pp pp pp
# information in the lines following the header have to look like this:
# ITEM: ATOMS id type x y z vx vy vz ix iy iz
module purge
module load intel/18 intel-mkl/2018 python/3.6 numpy/1.15.4 
plotlist=${1}
echo "Reading list of files from: ${plotlist}"
radius_split=${2}
python3 scripts/print_box_volume.py "${plotlist}"


