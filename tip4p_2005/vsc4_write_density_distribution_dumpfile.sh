#!/bin/bash
#
#SBATCH -J density-distribution
#SBATCH -N 1
#SBATCH --ntasks-per-node=16
#SBATCH --ntasks-per-core=1
#SBATCH --partition=mem_0096
#SBATCH --qos=devel_0096
##SBATCH --time=00:30:00

# data analysis
# infile = lammps dump file
# the box dimensions are taken from the file header
# ITEM: BOX BOUNDS pp pp pp
# information in the lines following the header have to look like this:
# ITEM: ATOMS id type x y z vx vy vz ix iy iz
# outfile is saved to ./data as "<posfile_info>_dichteverteilung.svg"

module purge
module load gcc/4.8.5 intel-mkl/2019 python/2.7.11 numpy/1.16.5
#module load intel/18 intel-mkl/2018 python/3.6 numpy/1.15.4 matplotlib/3.0.2
plotlist=${1}
echo "Reading list of files from: ${plotlist}"
radius_split=${2}
echo "Splitting radius in ${radius_split} equally distant parts"
while read line; do
   echo -e "\nProcessing file ${line}"
   infile_1=${line}
   radius=${infile_1##*radius}
   radius=${radius%%-*}
   radius=${radius##*_}
#   echo "Radius found: $radius"
   python scripts/density_distribution-dumpfile.py "${infile_1}" "${radius}" "${radius_split}"
done < ${plotlist}


