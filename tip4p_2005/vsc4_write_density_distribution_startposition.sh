#!/bin/bash
#
#SBATCH -J density-distribution-startpositions
#SBATCH -N 1
#SBATCH --ntasks-per-node=24
#SBATCH --ntasks-per-core=1
#SBATCH --mail-type=BEGIN    # first have to state the type of event to occur 
#SBATCH --partition=mem_0096
#SBATCH --qos=mem_0096 

# when srun is used, you need to set:
#srun -l -N2 -n32 a.out
# or
#mpirun -np 32 a.out
np=24


# data analysis
# infile = lammps dump file
# the box dimensions are taken from the file header
# ITEM: BOX BOUNDS pp pp pp
# information in the lines following the header have to look like this:
# ITEM: ATOMS id type x y z vx vy vz ix iy iz
# outfile is saved to ./data as "<posfile_info>_dichteverteilung.svg"
module purge
module load intel/18 intel-mkl/2018 python/3.6 numpy/1.15.4 matplotlib/3.0.2
plotlist=${1}
echo "Reading list of files from: ${plotlist}"
radius_split=${2}
echo "Splitting radius in ${radius_split} equally distant parts"
while read line; do
   echo -e "\nNow plotting data from file ${line}"
   infile_1=${line}
   python3 scripts/density_distribution-startposition.py "${infile_1}" "${radius_split}"
done < ${plotlist}


