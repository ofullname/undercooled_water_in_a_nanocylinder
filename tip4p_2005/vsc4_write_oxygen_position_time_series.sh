#!/bin/bash
#
#SBATCH -J poswriter
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --ntasks-per-core=2
#SBATCH --partition=mem_0128
#SBATCH --qos=devel_0128
#SBATCH --time=0:10:00

# when srun is used, you need to set:
#srun -l -N2 -n32 a.out
# or
#mpirun -np 32 a.out
np=8


# infile = lammps dump file
# the box dimensions are taken from the file header
# ITEM: BOX BOUNDS pp pp pp
# information in the lines following the header have to look like this:
# ITEM: ATOMS id type x y z vx vy vz ix iy iz
module purge
module load gcc/7.3 intel-mkl/2018 python/3.7 numpy/1.15.4

filelist=${1}
number_molecules=${2}
time_step=${3}

python3 scripts/write_oxygen_position_timeseries.py "${filelist}" "${number_molecules}" "${time_step}"

